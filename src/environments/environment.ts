// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

import { fbConfig } from './fbConfig';

export const environment = {
  production: false,
  firebase: fbConfig,
  mocks: {
    all: false, // master switch (must be enabled for any mocks further down)
    fbDb: {
      all: false
    },
    login: {
      all: false // turns on all mocks (cant receive real data without login)
    },
    management: {
      all: false
    },
    reports: {
      all: false
    }
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
