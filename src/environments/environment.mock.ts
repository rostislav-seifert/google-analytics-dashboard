export const environment = {
  production: true,
  mocks: {
    all: true, // master switch (must be enabled for any mocks further down)
    fbDb: {
      all: true
    },
    login: {
      all: true // turns on all mocks (cant receive real data without login)
    },
    management: {
      all: true
    },
    reports: {
      all: true
    }
  },
  firebase: {
    apiKey: '',
    authDomain: '',
    databaseURL: 'https://secret-spark-MOCK.firebaseio.com',
    projectId: '',
    storageBucket: '',
    messagingSenderId: '',
    appId: ''
  }
};
