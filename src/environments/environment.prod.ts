import { fbConfig } from './fbConfig';

export const environment = {
  production: true,
  firebase: fbConfig,
  mocks: {
    all: false, // master switch (must be enabled for any mocks further down)
    fbDb: {
      all: false
    },
    login: {
      all: false // turns on all mocks (cant receive real data without login)
    },
    management: {
      all: false
    },
    reports: {
      all: false
    }
  }
};
