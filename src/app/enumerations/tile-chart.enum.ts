export enum TileChartType {
  line = 'line',
  bar = 'bar',
  pie = 'pie',
  table = 'table'
}
