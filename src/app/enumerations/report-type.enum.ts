export enum ReportType {
  traffic = 'traffic',
  pageVisits = 'pageVisits',
  originCountry = 'originCountry',
  originLocalGlobal = 'originLocalGlobal',
  bounceRate = 'bounceRate',
  device = 'device',
  operatingSystem = 'operatingSystem',
  browser = 'browser',
  eventCount = 'eventCount',
  eventsPerUser = 'eventsPerUser',
  eventsPerSession = 'eventsPerSession'
}
