export enum DateRangeEnum {
  Week = 'week',
  Month = 'month',
  ThreeMonths = '3months',
  Year = 'year',
  ThreeYears = '3years',
  Custom = 'custom'
}
