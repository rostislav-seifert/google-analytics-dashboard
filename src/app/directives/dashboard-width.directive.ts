import { Directive, ElementRef, OnInit } from '@angular/core';

/*
sets width of the dashboard wrap div, due to horizontal centering of the dashboard
 */
@Directive({
  selector: '[gadDashboardWidth]'
})
export class DashboardWidthDirective implements OnInit {
  htmlElement: HTMLElement;
  parentElement: HTMLElement;
  private TILE_MARGIN = 16;

  constructor(
    private elementRef: ElementRef
  ) {
  }

  ngOnInit(): void {
    this.resize();
    window.addEventListener('resize', this.resize.bind(this));
  }

  private resize(): void {
    this.htmlElement = this.elementRef.nativeElement as HTMLElement;
    this.parentElement = this.htmlElement.parentElement;
    const parentWidth = this.parentElement.offsetWidth;
    const tileElement = this.htmlElement.childNodes[0] as HTMLElement;
    const tileInnerWidth = tileElement?.offsetWidth;
    if (tileInnerWidth) { // is there any tile on the dashboard?
      const tileWidth = tileInnerWidth + this.TILE_MARGIN;
      const width = Math.floor(parentWidth / tileWidth) * tileWidth;
      this.htmlElement.setAttribute('style', `width: ${width.toString()}px`);
    }
    else {
      // no tiles - act like there is one
      const tileWidth = this.TILE_MARGIN + 400;
      const width = Math.floor(parentWidth / tileWidth) * tileWidth;
      this.htmlElement.setAttribute('style', `width: ${width.toString()}px`);
    }
  }

}
