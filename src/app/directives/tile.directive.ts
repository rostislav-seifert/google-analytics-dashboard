import { Directive, ViewContainerRef } from '@angular/core';

@Directive({
  selector: '[gadTileHost]'
})
export class TileDirective {

  constructor(
    public viewContainerRef: ViewContainerRef
  ) { }

}
