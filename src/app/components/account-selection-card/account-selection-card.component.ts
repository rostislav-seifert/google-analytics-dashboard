import { Component, ElementRef, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges, ViewChild } from '@angular/core';
import { MAT_RADIO_DEFAULT_OPTIONS } from '@angular/material/radio';

@Component({
  selector: 'gad-account-selection-card',
  templateUrl: './account-selection-card.component.html',
  styleUrls: ['./account-selection-card.component.scss'],
  providers: [{
    provide: MAT_RADIO_DEFAULT_OPTIONS,
    useValue: { color: 'primary' }
  }]
})
export class AccountSelectionCardComponent implements OnInit, OnChanges {
  initialize = false;
  selectedItemId: string;
  filteredItems: SelectionCardItem[];

  @ViewChild('search') searchInput: ElementRef;

  @Input() disabled: boolean;
  @Input() title: string;
  @Input() items: SelectionCardItem[];

  @Output() selectItem = new EventEmitter<string>();

  constructor() { }

  ngOnInit(): void {
    this.filteredItems = this.items;
    this.initialize = true;
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (this.initialize && changes.items) {
      this.selectedItemId = null;
      this.searchInput.nativeElement.value = '';
      this.filteredItems = this.items;
    }
  }

  onItemSelect(): void {
    if (this.selectedItemId.length) {
      this.selectItem.emit(this.selectedItemId[0]);
    }
  }

  applyFilter(event: Event): void {
    this.selectedItemId = null;
    this.selectItem.emit(null);
    const filterValue = (event.target as HTMLInputElement).value.trim().toLowerCase();
    this.filteredItems = this.items.filter(
      // find name or id
      item => item.displayName.toLowerCase().includes(filterValue) || item.id.toLowerCase().includes(filterValue)
    );
  }

  showItems(): boolean {
    // and hide loading spinner
    return this.initialize;
  }

}

export interface SelectionCardItem {
  id: string;
  displayName: string;
}
