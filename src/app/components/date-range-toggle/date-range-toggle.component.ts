import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import * as moment from 'moment';

import { DateRangeEnum } from '../../enumerations/date-range.enum';
import { DateRange } from '../../models/date-range';
import { ReportsDataService } from '../../services/reports-data.service';

@Component({
  selector: 'gad-date-range-toggle',
  templateUrl: './date-range-toggle.component.html',
  styleUrls: ['./date-range-toggle.component.scss']
})
export class DateRangeToggleComponent implements OnInit, OnChanges {
  valuePool = [
    { range: DateRangeEnum.Week, tooltip: 'Week', text: 'W' },
    { range: DateRangeEnum.Month, tooltip: 'Month', text: 'M' },
    { range: DateRangeEnum.ThreeMonths, tooltip: '3 months', text: '3M' },
    { range: DateRangeEnum.Year, tooltip: 'Year', text: 'Y' },
    { range: DateRangeEnum.ThreeYears, tooltip: '3 years', text: '3Y' }
  ];
  selectValues = [];
  initialize = false;
  value;

  @Input() inputValue: DateRangeEnum = null;
  @Input() fullMode: boolean;
  @Output() selectOutput = new EventEmitter<DateRange>();

  constructor() { }

  ngOnInit(): void {
    this.value = this.inputValue || DateRangeEnum.Custom;
    this.initDateRanges();
    this.initialize = true;
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (this.initialize) {
      if (changes.inputValue) {
        this.value = this.inputValue;
        if (this.value === null) {
          this.value = 'custom';
        }
      }
      if (changes.fullMode) {
        this.initDateRanges();
      }
    }
  }

  onTypeChange(selectedValue: DateRangeEnum): void {
    if (selectedValue !== 'custom') {
      let from: moment.Moment;
      switch (selectedValue) {
        case DateRangeEnum.Week:
          from = moment().subtract(1, 'week');
          break;
        case DateRangeEnum.Month:
          from = moment().subtract(1, 'month');
          break;
        case DateRangeEnum.ThreeMonths:
          from = moment().subtract(3, 'month');
          break;
        case DateRangeEnum.Year:
          from = moment().subtract(1, 'year');
          break;
        case DateRangeEnum.ThreeYears:
          from = moment().subtract(3, 'years');
          break;
        default:
          break;
      }
      const range: DateRange = {
        from: from.format(ReportsDataService.REPORT_DATE_FORMAT),
        to: moment().format(ReportsDataService.REPORT_DATE_FORMAT),
        range: selectedValue
      };
      this.selectOutput.emit(range);
    }
  }

  onCustomClick(event): void {
    event.stopPropagation();
  }

  private initDateRanges(): void {
    if (this.fullMode) {
      this.selectValues = this.valuePool;
    }
    else {
      this.selectValues = this.valuePool.filter(value => ![DateRangeEnum.ThreeMonths, DateRangeEnum.ThreeYears].includes(value.range));
    }
  }

}
