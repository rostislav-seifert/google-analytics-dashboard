import { ChangeDetectorRef, Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Chart } from 'chart.js';
import { Observable, of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { saveAs } from 'file-saver';
import { BreakpointObserver } from '@angular/cdk/layout';

import { DateRange } from '../../models/date-range';
import { ReportType } from '../../enumerations/report-type.enum';
import { TileChartType } from '../../enumerations/tile-chart.enum';
import { TileConfigurationService } from '../../services/tile-configuration.service';
import { TileModalConfig } from '../../models/tile-modal-config';
import { AnalyticsService } from '../../services/analytics.service';
import { ReportResponse } from '../../api/models/reportResponse/report-response';
import { ReportsDataService } from '../../services/reports-data.service';
import { ReportRequest } from '../../api/models/report-request';
import { TileMode } from '../../enumerations/tile-mode.enum';
import { ChartSettingsService } from '../../services/chart-settings.service';
import { ChartData } from '../../models/chart-data';

@Component({
  selector: 'gad-tile-modal',
  templateUrl: './tile-modal.component.html',
  styleUrls: ['./tile-modal.component.scss']
})
export class TileModalComponent implements OnInit {
  initialize = false;
  touchDevice = false;
  portrait = false;
  changes = false;
  noData = false;
  reportRequest: ReportRequest = {};
  tileReportType: ReportType;
  tileRange: DateRange;
  tileChartType: TileChartType;
  table = false;
  tableDateColumn = false;
  disabledChartTypes: TileChartType[];

  // chart params
  chartHeight = 445;
  chartType: Chart.ChartType = 'line';
  chartData: ChartData[];
  chartTooltips: string[];
  chartLabels = [];
  chartOptions: Chart.ChartOptions = {
    title: {
      text: 'Users last 30 days',
      display: false
    },
    legend: {
      position: 'top',
      display: true
    },
    scales: {
      yAxes: [{
        ticks: { beginAtZero: true }
      }]
    },
    tooltips: {
      callbacks: {
        label: Chart.defaults.global.tooltips.callbacks.label,
        title: Chart.defaults.global.tooltips.callbacks.title
      }
    },
    maintainAspectRatio: false,
    responsive: true
  };

  constructor(
    public dialogRef: MatDialogRef<TileModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: TileModalConfig,
    private configService: TileConfigurationService,
    private analyticsService: AnalyticsService,
    private changeDetectorRef: ChangeDetectorRef,
    private reportsDataService: ReportsDataService,
    private breakpointObserver: BreakpointObserver
  ) {
    // chartJs plugin - set canvas background color to white (for download, default was transparent with png or black with jpeg)
    Chart.plugins.register({
      beforeDraw: (c: any) => {
        const ctx = c.chart.ctx;
        ctx.fillStyle = 'white';
        ctx.fillRect(0, 0, c.chart.width, c.chart.height);
      }
    });

    this.touchDevice = this.isTouchDevice();
  }

  ngOnInit(): void {
    // unpack input
    this.tileReportType = this.data.reportType;
    this.tileChartType = this.data.tileChartType;
    this.tileRange = this.data.dateRange;

    const portrait = this.breakpointObserver.observe([
      '(orientation: portrait)'
    ]);
    const landscape = this.breakpointObserver.observe([
      '(orientation: landscape)'
    ]);

    portrait.subscribe(result => {
      if (result.matches) {
        this.portrait = true;
        this.chartHeight = 445;
        if (this.isTouchDevice()) {
          this.chartOptions.legend.position = 'top';
        }
      }
    });
    landscape.subscribe(result => {
      if (result.matches && this.isTouchDevice()) {
        this.portrait = false;
        this.chartHeight = 150;
        this.chartOptions.legend.position = 'right';
      }
    });

    this.disabledChartTypes = this.configService.disabledCharts(this.tileReportType);
    this.initReportRequest();

    if (!this.isTouchDevice()) {
      this.chartOptions.legend.position = 'right';
    }

    this.getData().subscribe(() => {
      this.initialize = true;
    });
  }

  onReportTypeChange(val: ReportType): void {
    this.tileReportType = val;
    this.updateChangedStatus();

    // set appropriate chart for the selected report
    this.disabledChartTypes = this.configService.disabledCharts(val);
    const defaultChartType = this.configService.recommendedChart(val);
    this.onChartTypeChange(defaultChartType);
    this.setReportType();
    this.getData().subscribe();
  }

  onDateRangeSelect(range: DateRange): void {
    this.tileRange = range;
    this.updateChangedStatus();
    this.reportRequest.dateRanges[0].startDate = range.from;
    this.reportRequest.dateRanges[0].endDate = range.to;
    this.getData().subscribe();
  }

  onChartTypeChange(val: TileChartType): void {
    this.tileChartType = val;
    this.updateChangedStatus();
    this.setChartType();
  }

  onSave(): void {
    const result: TileModalConfig = {
      reportType: this.tileReportType,
      tileChartType: this.tileChartType,
      dateRange: this.tileRange
    };
    this.dialogRef.close(result);
  }

  onClose(): void {
    this.dialogRef.close();
  }

  onSaveImage(): void {
    const canvas = document.getElementById('canvas') as HTMLCanvasElement;
    canvas.toBlob(blob => {
      saveAs(blob, 'image');
    }, 'image/jpeg');
  }

  disableDownload(): boolean {
    return this.tileChartType === TileChartType.table;
  }

  private getData(): Observable<void> {
    this.initialize = false;
    delete this.reportRequest.pageToken;
    return this.analyticsService.getData(this.reportRequest).pipe(
      catchError(() => of(null)),
      map((responseBody: ReportResponse) => {
        if (responseBody && responseBody.reports && responseBody.reports[0] && responseBody.reports[0].rows) {
          this.noData = false;
          const tileChartData = this.reportsDataService.formatData(responseBody, this.tileReportType, TileMode.full);
          this.chartData = tileChartData.datasets;
          this.setChartColors();
          this.chartLabels = tileChartData.labels;
          if (tileChartData.tooltips) {
            this.chartTooltips = tileChartData.tooltips;
            this.chartOptions.tooltips.callbacks.title = this.tooltipTitleCallback;
            this.chartOptions.tooltips.callbacks.label = this.tooltipLabelCallback;
            this.chartOptions.tooltips.displayColors = false;
            this.chartOptions.tooltips.bodyAlign = 'right';
          }
          else {
            this.chartTooltips = null;
            delete this.chartOptions.tooltips.callbacks.title;
            delete this.chartOptions.tooltips.callbacks.label;
            this.chartOptions.tooltips.displayColors = true;
            this.chartOptions.tooltips.bodyAlign = Chart.defaults.global.tooltips.bodyAlign;
          }
          this.changeDetectorRef.detectChanges();
        }
        else {
          this.noData = true;
        }
        this.initialize = true;
      })
    );
  }

  private initReportRequest(): void {
    // clear previous request parameters
    this.reportRequest = {};

    this.reportRequest.dateRanges = [{
      startDate: this.tileRange.from,
      endDate: this.tileRange.to
    }];

    // set values from input
    this.initReportRequestFromInput();
  }

  private initReportRequestFromInput(): void {
    this.setReportType();
    this.setChartType();
  }

  private setReportType(): void {
    const config = this.configService.configMetricDimension(this.tileReportType);
    this.reportRequest.metrics = config.metrics;
    this.reportRequest.dimensions = config.dimensions;
  }

  private setChartType(): void {
    if (this.table) {
      this.table = false;
    }
    if (this.chartData && this.chartData[0] && this.tileChartType !== TileChartType.table) {
      this.setChartColors();
    }
    switch (this.tileChartType) {
      case TileChartType.bar:
        this.chartOptions.scales.yAxes[0].display = true;
        this.chartType = 'bar';
        this.chartOptions.legend.display = false;
        break;
      case TileChartType.line:
        this.chartOptions.scales.yAxes[0].display = true;
        this.chartType = 'line';
        this.chartOptions.legend.display = true;
        break;
      case TileChartType.pie:
        this.chartOptions.scales.yAxes[0].display = false;
        this.chartType = 'pie';
        this.chartOptions.legend.display = true;
        break;
      case TileChartType.table:
        this.table = true;
        this.tableDateColumn = this.reportsDataService.hasDateColumn(this.tileReportType);
        break;
      default:
        break;
    }
  }

  private updateChangedStatus(): void {
    if (this.tileReportType === this.data.reportType && this.tileChartType === this.data.tileChartType) {
      // same report and chart -> compare dates
      if (this.data.dateRange.range === null) {
        // null range (D, M, Y) -> compare date from to
        this.changes = !(this.tileRange.from === this.data.dateRange.from && this.tileRange.to === this.data.dateRange.to);
      }
      else {
        // range has value (D, M, Y) -> priority over from to
        this.changes = this.tileRange.range !== this.data.dateRange.range;
      }
    }
    else {
      this.changes = true;
    }
  }

  private isTouchDevice(): boolean {
    return (('ontouchstart' in window) ||
      (navigator.maxTouchPoints > 0) ||
      (navigator.msMaxTouchPoints > 0));
  }

  private tooltipTitleCallback = (item: Chart.ChartTooltipItem[], data: Chart.ChartData): string | string[] => {
    const text = this.chartTooltips[item[0].index];
    let linedText = '';
    for (let i = 0; i < text.length; i++) {
      linedText = linedText.concat(text[i]);
      if (i % 58 === 0 && i > 0) {
        linedText = linedText.concat('\n');
      }
    }
    return linedText;
  };

  private tooltipLabelCallback =
    (tooltipItem: Chart.ChartTooltipItem, data: Chart.ChartData): string | string[] => {
      if ([ReportType.traffic, ReportType.bounceRate].includes(this.tileReportType)) {
        return `average: ${tooltipItem.yLabel.toString()}`;
      }
      if (this.chartType === TileChartType.pie) {
        const val = data.datasets[0].data[tooltipItem.index] as number;
        const valArr = data.datasets[0].data as number[];
        const sum = valArr.reduce((a, b) => a + b, 0);
        const roundedValPerc = Math.round((val / sum) * 10000) / 100 ;
        return `${roundedValPerc}% - ${val}`;
      }
      return `${tooltipItem.yLabel.toString()}`;
    };

  private setChartColors(): void {
    switch (this.tileChartType) {
      case TileChartType.bar:
        this.chartData[0].backgroundColor = ChartSettingsService.generateColorArray(this.chartData[0].data.length);
        this.chartData[0].borderColor = 'white';
        break;
      case TileChartType.line:
        this.chartData[0].backgroundColor = Chart.defaults.global.defaultColor as string;
        this.chartData[0].borderColor = ChartSettingsService.lineColor;
        this.chartData[0].pointBackgroundColor = ChartSettingsService.lineColor;
        this.chartData[0].pointBorderColor = 'white';
        break;
      case TileChartType.pie:
        this.chartData[0].backgroundColor = ChartSettingsService.generateColorArray(this.chartData[0].data.length);
        this.chartData[0].borderColor = 'white';
        break;
      default:
        break;
    }
  }
}
