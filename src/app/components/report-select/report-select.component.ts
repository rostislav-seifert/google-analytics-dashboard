import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';

import { ReportType } from '../../enumerations/report-type.enum';

@Component({
  selector: 'gad-report-select',
  templateUrl: './report-select.component.html',
  styleUrls: ['./report-select.component.scss']
})
export class ReportSelectComponent implements OnInit, OnChanges {
  selectValuesGA = [
    { type: ReportType.traffic, text: 'Site traffic' },
    { type: ReportType.pageVisits, text: 'Page traffic' },
    { type: ReportType.originCountry, text: 'Traffic by country' },
    { type: ReportType.originLocalGlobal, text: 'Traffic local / global' },
    { type: ReportType.bounceRate, text: 'Bounce rate' },
    { type: ReportType.device, text: 'Device category' },
    { type: ReportType.operatingSystem, text: 'Operating system' },
    { type: ReportType.browser, text: 'Browser' }
  ];
  selectValuesGTM = [
    { type: ReportType.eventCount, text: 'Events Total' },
    { type: ReportType.eventsPerUser, text: 'Events Per User' },
    { type: ReportType.eventsPerSession, text: 'Event Per Session' }
  ];

  @Input() inputValue: ReportType = null;
  @Output() selectOutput = new EventEmitter<ReportType>();
  value = ReportType.traffic;

  constructor() { }

  ngOnInit(): void {
    if (this.inputValue) {
      this.value = this.inputValue;
    }
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.inputValue) {
      this.value = this.inputValue;
    }
  }

  onTypeChange(selectedType: ReportType): void {
    this.selectOutput.emit(selectedType);
  }

}
