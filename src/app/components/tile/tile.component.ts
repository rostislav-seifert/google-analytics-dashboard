import { ChangeDetectorRef, Component, Input, OnInit, ViewChild } from '@angular/core';
import { Store } from '@ngrx/store';
import { Chart } from 'chart.js';
import { Observable, of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import * as moment from 'moment';
import { BreakpointObserver } from '@angular/cdk/layout';

import { TileModalComponent } from '../tile-modal/tile-modal.component';
import { AppState } from '../../store';
import { AnalyticsService } from '../../services/analytics.service';
import { TileChartType } from '../../enumerations/tile-chart.enum';
import { ReportsDataService } from '../../services/reports-data.service';
import { ReportType } from '../../enumerations/report-type.enum';
import { ReportRequest } from '../../api/models/report-request';
import { DeleteTile, SetTileChart, SetTileDateRange, SetTileReport } from '../../store/actions/dashboard.actions';
import { TileConfig } from '../../models/tile-config';
import { ChartTypeSelectComponent } from '../chart-type-select/chart-type-select.component';
import { TileConfigurationService } from '../../services/tile-configuration.service';
import { DateRange } from '../../models/date-range';
import { TileModalConfig } from '../../models/tile-modal-config';
import { ReportResponse } from '../../api/models/reportResponse/report-response';
import { AreYouSureModalComponent } from '../are-you-sure-modal/are-you-sure-modal.component';
import { TileMode } from '../../enumerations/tile-mode.enum';
import { ChartSettingsService } from '../../services/chart-settings.service';
import { ChartData } from '../../models/chart-data';

@Component({
  selector: 'gad-tile',
  templateUrl: './tile.component.html',
  styleUrls: ['./tile.component.scss']
})
export class TileComponent implements OnInit {
  @Input() data: TileConfig;

  _ref: any;
  initialize = false;
  noData = false;
  reportRequest: ReportRequest = {};
  tileReportType: ReportType;
  tileChartType: TileChartType;
  table = false;
  tableDateColumn = false;
  disabledChartTypes: TileChartType[];
  tileRange: DateRange;

  // chart params
  chartType: Chart.ChartType = 'line';
  chartData: ChartData[];
  chartTooltips: string[];
  chartLabels = [];
  chartOptions: Chart.ChartOptions = {
    title: {
      text: '',
      position: 'bottom',
      display: true,
      fontFamily: 'Roboto, "Helvetica Neue", sans-serif',
      fontSize: 14,
      fontColor: 'black',
      fontStyle: 'normal',
      padding: 5
    },
    scales: {
      yAxes: [{
        ticks: { beginAtZero: true }
      }],
      xAxes: [{
        ticks: { fontColor: 'black' }
      }]
    },
    tooltips: {
      // custom: Chart.defaults.global.tooltips.custom
      callbacks: {
        // overriding later, for specific reports / chart types
        label: Chart.defaults.global.tooltips.callbacks.label,
        title: Chart.defaults.global.tooltips.callbacks.title
      }
    },
    legend: { display: true },
    maintainAspectRatio: false,
    responsive: true
  };
  isSmallScreen: boolean;

  @ViewChild(ChartTypeSelectComponent)
  private chartTypeSelectComponent: ChartTypeSelectComponent;

  constructor(
    private store: Store<AppState>,
    private analyticsService: AnalyticsService,
    private changeDetectorRef: ChangeDetectorRef,
    private reportsDataService: ReportsDataService,
    private configService: TileConfigurationService,
    private dialog: MatDialog,
    private breakpointObserver: BreakpointObserver
  ) {
    this.isSmallScreen = breakpointObserver.isMatched('(max-width: 599px)');
  }

  ngOnInit(): void {
    this.tileChartType = this.data.chart || TileChartType.line;
    this.tileRange = this.data && this.data.dateRange || TileConfigurationService.DEFAULT_DATE_RANGE;
    this.initReportRequest(true);
    this.tileReportType = this.data && this.data.report || ReportType.traffic;
    const inputReport = this.data.report;
    if (inputReport) {
      this.setReportType(inputReport);
    }
    this.disabledChartTypes = this.configService.disabledCharts(this.tileReportType);
    this.getData().subscribe(() => {
      this.initialize = true;
      this.changeDetectorRef.detectChanges();
    });
  }

  onReportTypeChange(val: ReportType, fromModal = false): void {
    this.initReportRequest(); // reset request parameters
    // duplicate call - called in initReportRequest also, but with data.report -> need to overwrite with new "val" here
    this.setReportType(val);
    this.store.dispatch(new SetTileReport({ index: this.data.index, reportType: this.tileReportType }));

    // disable unsuitable chart types for the selected report
    this.disabledChartTypes = this.configService.disabledCharts(val);

    if (!fromModal) {
      // set appropriate chart for the selected report
      const defaultChartType = this.configService.recommendedChart(val);
      this.onChartTypeChange(defaultChartType);

      this.getData().subscribe();
    }
  }

  onChartTypeChange(val: TileChartType): void {
    this.setChartType(val);
    this.store.dispatch(new SetTileChart({ index: this.data.index, chartType: val }));
  }

  onDateSelect(range: DateRange, fromModal = false): void {
    this.tileRange = range;
    this.reportRequest.dateRanges[0].startDate = range.from;
    this.reportRequest.dateRanges[0].endDate = range.to;
    this.store.dispatch(new SetTileDateRange({ index: this.data.index, date: range }));
    this.chartOptions.title.text = `${moment(range.from).format(ReportsDataService.TITLE_DATE_FORMAT)} - ${moment(range.to).format(ReportsDataService.TITLE_DATE_FORMAT)}`;
    if (!fromModal) {
      this.getData().subscribe();
    }
  }

  onFullScreenClick(): void {
    const inputData: TileModalConfig = {
      reportType: this.tileReportType,
      tileChartType: this.tileChartType,
      dateRange: this.tileRange
    };
    const config: MatDialogConfig = {
      data: inputData,
      width: '100%',
      height: '700px'
    };
    if (this.isTouchDevice()) {
      config.minWidth = '98vw';
      config.panelClass = 'tile-modal-custom';
    }
    const dialogRef = this.dialog.open(TileModalComponent, config);

    // react on changes made in the dialog, only on positive result. React on click outside is possible:
    // https://stackoverflow.com/questions/54408225/how-do-i-use-the-backdropclick-of-a-mat-dialog-to-close-the-dialog-and-send-the
    dialogRef.afterClosed().subscribe((result: TileModalConfig) => {
      if (result) {
        // apply all changes to the from modal
        this.applyModalChanges(result);
      }
    });
  }

  onChartClick(event: Event): void {
    // disable full tile through chart click (btn still works),
    // and let the chart handle it on mobile devices (show chart labels on touch)
    if (this.isTouchDevice()) {
      event.stopPropagation();
    }
  }

  // delete button
  destroy(): void {
    const dialogRef = this.dialog.open(AreYouSureModalComponent, { width: '250px' });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.store.dispatch(new DeleteTile(this.data.index));
        this._ref.destroy();
      }
    });
  }

  private getData(): Observable<void> {
    this.initialize = false;
    return this.analyticsService.getData(this.reportRequest).pipe(
      catchError(() => of(null)),
      map((responseBody: ReportResponse) => {
        if (responseBody && responseBody.reports && responseBody.reports[0] && responseBody.reports[0].rows) {
          this.noData = false;
          const tileChartData = this.reportsDataService.formatData(responseBody, this.tileReportType, TileMode.basic);
          this.chartData = tileChartData.datasets;
          this.setChartColors();
          this.chartLabels = tileChartData.labels;
          if (tileChartData.tooltips) {
            this.chartTooltips = tileChartData.tooltips;
            this.chartOptions.tooltips.callbacks.title = this.tooltipTitleCallback;
            this.chartOptions.tooltips.callbacks.label = this.tooltipLabelCallback;
            this.chartOptions.tooltips.displayColors = false;
            this.chartOptions.tooltips.bodyAlign = 'right';
          }
          else {
            this.chartTooltips = null;
            delete this.chartOptions.tooltips.callbacks.title;
            delete this.chartOptions.tooltips.callbacks.label;
            this.chartOptions.tooltips.displayColors = true;
            this.chartOptions.tooltips.bodyAlign = Chart.defaults.global.tooltips.bodyAlign;
          }
          this.changeDetectorRef.detectChanges();
        }
        else {
          this.noData = true;
        }
        this.initialize = true;
      })
    );
  }

  private applyModalChanges(result: TileModalConfig): void {
    if (result.reportType !== this.tileReportType) {
      this.tileReportType = result.reportType;
      this.onReportTypeChange(result.reportType, true);
    }
    if (result.tileChartType !== this.tileChartType) {
      this.tileChartType = result.tileChartType;
      this.onChartTypeChange(this.tileChartType);
    }
    if (result.dateRange && !this.equalDateRanges(this.tileRange, result.dateRange)) {
      this.onDateSelect(result.dateRange, true);
    }

    this.getData().subscribe();
  }

  private initReportRequest(initial = false): void {
    // clear previous request parameters
    this.reportRequest = {};

    // set default values
    this.reportRequest.dateRanges = [
      {
        startDate: this.tileRange.from,
        endDate: this.tileRange.to
      }
    ];
    this.reportRequest.metrics = [{
      name: 'activeUsers'
      // expression: 'ga:users'
    }];
    this.reportRequest.dimensions = [{ name: 'date' }]; // dimension explorer for more

    // set values from input
    if (initial) {
      this.initReportRequestFromInput();
      this.chartOptions.title.text =
        `${moment(this.tileRange.from).format(ReportsDataService.TITLE_DATE_FORMAT)} - ${moment(this.tileRange.to).format(ReportsDataService.TITLE_DATE_FORMAT)}`;
    }
  }

  private initReportRequestFromInput(): void {
    // data.report OK for now - for manual onSelectChange -> overwritten
    const inputReport = this.data.report;
    if (inputReport) {
      this.setReportType(inputReport);
    }
    const inputChart = this.tileChartType;
    if (inputChart) {
      this.setChartType(inputChart);
    }
  }

  private setReportType(reportType: ReportType): void {
    this.tileReportType = reportType;
    const config = this.configService.configMetricDimension(reportType);
    this.reportRequest.metrics = config.metrics;
    this.reportRequest.dimensions = config.dimensions;
  }

  private setChartType(val: TileChartType): void {
    this.tileChartType = val;
    if (this.table) {
      this.table = false;
    }
    if (this.chartData && this.chartData[0] && this.tileChartType !== TileChartType.table) {
      this.setChartColors();
    }
    switch (val) {
      case TileChartType.bar:
        this.chartOptions.scales.yAxes[0].display = true;
        this.chartOptions.scales.xAxes[0].display = true;
        this.chartOptions.legend.display = false;
        this.chartType = 'bar';
        break;
      case TileChartType.line:
        this.chartOptions.scales.yAxes[0].display = true;
        this.chartOptions.scales.xAxes[0].display = true;
        this.chartOptions.legend.display = true;
        this.chartType = 'line';
        break;
      case TileChartType.pie:
        this.chartOptions.scales.yAxes[0].display = false;
        this.chartOptions.scales.xAxes[0].display = false;
        this.chartOptions.legend.display = false;
        this.chartType = 'pie';
        break;
      case TileChartType.table:
        this.table = true;
        this.tableDateColumn = this.reportsDataService.hasDateColumn(this.tileReportType);
        break;
      default:
        break;
    }
  }

  private equalDateRanges(rangeA: DateRange, rangeB: DateRange): boolean {
    return rangeA.from === rangeB.from && rangeA.to === rangeB.to && rangeA.range === rangeB.range;
  }

  private setChartColors(): void {
    switch (this.tileChartType) {
      case TileChartType.bar:
        this.chartData[0].backgroundColor = ChartSettingsService.generateColorArray(this.chartData[0].data.length);
        this.chartData[0].borderColor = 'white';
        break;
      case TileChartType.line:
        this.chartData[0].backgroundColor = Chart.defaults.global.defaultColor as string;
        this.chartData[0].borderColor = ChartSettingsService.lineColor;
        this.chartData[0].pointBackgroundColor = ChartSettingsService.lineColor;
        this.chartData[0].pointBorderColor = 'white';
        break;
      case TileChartType.pie:
        this.chartData[0].backgroundColor = ChartSettingsService.generateColorArray(this.chartData[0].data.length);
        this.chartData[0].borderColor = 'white';
        break;
      default:
        break;
    }
  }

  private tooltipTitleCallback = (item: Chart.ChartTooltipItem[], data: Chart.ChartData): string | string[] => {
    const text = this.chartTooltips[item[0].index];
    let linedText = '';
    for (let i = 0; i < text.length; i++) {
      linedText = linedText.concat(text[i]);
      if (i % 58 === 0 && i > 0) {
        linedText = linedText.concat('\n');
      }
    }
    return linedText;
  };

  private tooltipLabelCallback =
    (tooltipItem: Chart.ChartTooltipItem, data: Chart.ChartData): string | string[] => {
      if ([ReportType.traffic, ReportType.bounceRate].includes(this.tileReportType)) {
        return `average: ${tooltipItem.yLabel.toString()}`;
      }
      if (this.chartType === TileChartType.pie) {
        const val = data.datasets[0].data[tooltipItem.index] as number;
        const valArr = data.datasets[0].data as number[];
        const sum = valArr.reduce((a, b) => a + b, 0);
        const roundedValPerc = Math.round((val / sum) * 10000) / 100 ;
        return `${roundedValPerc}% - ${val}`;
      }
      return `${tooltipItem.yLabel.toString()}`;
    };

  private isTouchDevice(): boolean {
    return (('ontouchstart' in window) ||
      (navigator.maxTouchPoints > 0) ||
      (navigator.msMaxTouchPoints > 0));
  }

}
