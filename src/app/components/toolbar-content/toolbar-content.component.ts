import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';

import { UserService } from '../../services/user.service';
import { ToolbarService } from '../../services/toolbar.service';

@Component({
  selector: 'gad-toolbar-content',
  templateUrl: './toolbar-content.component.html',
  styleUrls: ['./toolbar-content.component.scss']
})
export class ToolbarContentComponent implements OnInit {
  isSmallScreen: boolean;

  constructor(
    private userService: UserService,
    private toolbarService: ToolbarService,
    private router: Router,
    private breakpointObserver: BreakpointObserver
  ) {
    this.breakpointObserver.observe([Breakpoints.XSmall, Breakpoints.Small]).subscribe(result => this.isSmallScreen = result.matches);
  }

  ngOnInit(): void {
  }

  onAddTile(): void {
    this.toolbarService.addTileClick();
  }

  onClearDashboard(): void {
    this.toolbarService.clearDashboardClick();
  }

  onAccountSelection(): void {
    this.router.navigate(['account-selection']);
  }

  signOut(): void {
    this.userService.signOut();
  }

  isLoggedIn(): boolean {
    return this.userService.isUserSignedIn;
  }

  showDashboardButtons(): boolean {
    return this.router.url.split('?')[0] === '/dashboard';
  }

}
