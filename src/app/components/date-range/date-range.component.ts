import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import * as moment from 'moment';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { MAT_MOMENT_DATE_ADAPTER_OPTIONS, MomentDateAdapter } from '@angular/material-moment-adapter';

import { DateValidators } from '../../validators/date-validators';
import { DateRange } from '../../models/date-range';
import { ReportsDataService } from '../../services/reports-data.service';

export const CUSTOM_FORMATS = {
  parse: {
    dateInput: 'DD.MM.YYYY'
  },
  display: {
    dateInput: 'DD.MM.YYYY',
    monthYearLabel: 'MMMM YYYY'
  }
};

@Component({
  selector: 'gad-date-range',
  templateUrl: './date-range.component.html',
  styleUrls: ['./date-range.component.scss'],
  providers: [
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS]
    },
    { provide: MAT_DATE_FORMATS, useValue: CUSTOM_FORMATS }
  ]
})
export class DateRangeComponent implements OnInit, OnChanges {
  dateRangeForm: FormGroup;
  disableApplyDate = false;

  @Input() inputDateRange: DateRange;
  @Output() dateSelect = new EventEmitter<DateRange>();

  constructor() {
    this.dateRangeForm = new FormGroup({
      dateFrom: new FormControl(this.inputDateRange && this.inputDateRange.from || moment().subtract(1, 'month'),
        [DateValidators.futureDate()]),
      dateTo: new FormControl(this.inputDateRange && this.inputDateRange.to || moment(), [DateValidators.futureDate()])
    });
  }

  ngOnInit(): void {
    if (this.inputDateRange) {
      this.dateRangeForm.patchValue({
        dateFrom: moment.parseZone(this.inputDateRange.from, ReportsDataService.REPORT_DATE_FORMAT),
        dateTo: moment.parseZone(this.inputDateRange.to, ReportsDataService.REPORT_DATE_FORMAT)
      });
    }
    this.setDateFromValidator();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.inputDateRange) {
      this.dateRangeForm.patchValue({
        dateFrom: moment.parseZone(this.inputDateRange.from, ReportsDataService.REPORT_DATE_FORMAT),
        dateTo: moment.parseZone(this.inputDateRange.to, ReportsDataService.REPORT_DATE_FORMAT)
      });
    }
  }

  onDateFromChange(): void {
    this.updateApplyDateButton();
  }

  onDateTochange(): void {
    this.dateRangeForm.get('dateFrom').updateValueAndValidity();
    this.dateRangeForm.get('dateFrom').markAsTouched();
    this.updateApplyDateButton();
  }

  applyDateRange(): void {
    if (this.dateRangeForm.valid) {
      const dateFrom = this.dateRangeForm.get('dateFrom').value.format(ReportsDataService.REPORT_DATE_FORMAT);
      const dateTo = this.dateRangeForm.get('dateTo').value.format(ReportsDataService.REPORT_DATE_FORMAT);
      this.dateSelect.emit({ from: dateFrom, to: dateTo, range: null });
    }
  }

  // set 'from' after 'to' validator, cant set in constructor (form doesnt exist yet)
  private setDateFromValidator(): void {
    const fcDateFrom = this.dateRangeForm.get('dateFrom');
    const fcDateTo = this.dateRangeForm.get('dateTo');
    fcDateFrom.setValidators([DateValidators.futureDate(), DateValidators.afterDate(fcDateTo)]);
  }

  private updateApplyDateButton(): void {
    this.disableApplyDate = !this.dateRangeForm.valid;
  }
}
