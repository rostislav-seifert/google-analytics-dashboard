import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'gad-are-you-sure-modal',
  templateUrl: './are-you-sure-modal.component.html',
  styleUrls: ['./are-you-sure-modal.component.scss']
})
export class AreYouSureModalComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
