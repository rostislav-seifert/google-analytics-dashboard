import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'gad-clear-dashboard-modal',
  templateUrl: './clear-dashboard-modal.component.html',
  styleUrls: ['./clear-dashboard-modal.component.scss']
})
export class ClearDashboardModalComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
