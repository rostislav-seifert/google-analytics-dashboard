import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ChartTypeSelectComponent } from './chart-type-select.component';

describe('ChartTypeSelectComponent', () => {
  let component: ChartTypeSelectComponent;
  let fixture: ComponentFixture<ChartTypeSelectComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ChartTypeSelectComponent ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChartTypeSelectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
