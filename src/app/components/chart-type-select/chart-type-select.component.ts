import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';

import { TileChartType } from '../../enumerations/tile-chart.enum';

@Component({
  selector: 'gad-chart-type-select',
  templateUrl: './chart-type-select.component.html',
  styleUrls: ['./chart-type-select.component.scss']
})
export class ChartTypeSelectComponent implements OnInit, OnChanges {
  valuePool = [
    { type: TileChartType.bar, tooltip: 'Bar chart', icon: 'bar_chart' },
    { type: TileChartType.line, tooltip: 'Line chart', icon: 'show_chart' },
    { type: TileChartType.pie, tooltip: 'Pie chart', icon: 'pie_chart' },
    { type: TileChartType.table, tooltip: 'Table', icon: 'table_chart' }
  ];
  selectValues = [];
  initialize = false;
  value;

  @Input() inputValue: TileChartType = null;
  @Input() disabledTypes: TileChartType[];
  @Output() selectOutput = new EventEmitter<TileChartType>();

  constructor(
  ) { }

  ngOnInit(): void {
    this.value = this.inputValue;
    this.disableChartTypes();
    this.initialize = true;
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (this.initialize) {
      if (changes.inputValue) {
        this.value = this.inputValue;
      }
      if (changes.disabledTypes) {
        this.disableChartTypes();
      }
    }
  }

  onTypeChange(selectedValue: TileChartType): void {
    this.selectOutput.emit(selectedValue);
  }

  private disableChartTypes(): void {
    // filter all possible values, if value is not found in disabled values, it comes through the filter
    this.selectValues = this.valuePool.filter(value => !this.disabledTypes.some(disabled => disabled === value.type));
  }
}
