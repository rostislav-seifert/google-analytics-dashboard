import { AfterViewInit, Component, Input, OnChanges, OnInit, SimpleChanges, ViewChild } from '@angular/core';
import { MatTable, MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import * as moment from 'moment';

import { ReportType } from '../../enumerations/report-type.enum';
import { TileConfigurationService } from '../../services/tile-configuration.service';

enum TileTableMode {
  BASIC = 'BASIC',
  FULL = 'FULL'
}
interface TableData {
  column1: string;
  column2: any;
}

@Component({
  selector: 'gad-tile-table',
  templateUrl: './tile-table.component.html',
  styleUrls: ['./tile-table.component.scss']
})
export class TileTableComponent implements OnInit, OnChanges, AfterViewInit {
  @ViewChild(MatTable) table: MatTable<any>;
  @ViewChild(MatSort) sort: MatSort;

  @Input()
  chartData: { datasets: { data: any[], label?: string }[], labels: string[], tooltips: string[] };

  @Input()
  reportType: ReportType;

  @Input()
  mode: 'BASIC' | 'FULL' = TileTableMode.BASIC;

  @Input()
  isFirstColumnDate = false;

  tableData: TableData[] = []; // data converted to mat-table data format
  dataSource: MatTableDataSource<TableData>;
  displayedColumns: string[];
  headerLabels: TableData = { column1: '', column2: '' };
  initialize = false;

  constructor(
    private readonly tileConfigurationService: TileConfigurationService
  ) { }

  ngOnInit(): void {
    this.displayedColumns = ['column1', 'column2'];
    this.initialize = true;
  }

  ngAfterViewInit(): void {
    this.dataSource.sort = this.sort;
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.chartData) {
      this.chartData = changes.chartData.currentValue;
      this.initTableData();
      if (this.initialize) {
        this.table.renderRows();
      }
    }
  }

  stickyHeader(): boolean {
    return this.mode !== TileTableMode.BASIC;
  }

  enableSort(): boolean {
    return this.mode !== TileTableMode.BASIC;
  }

  formatNumber(): string {
    // return this.headerLabels.column2.endsWith('(%)') ? '2.2-2' : '1.0-0';
    return '1.0-0';
  }

  hasDateColumn(): boolean {
    return this.headerLabels.column1 === 'Date' && !this.chartData.tooltips;
  }

  private initTableData(): void {
    this.tableData = [];
    const maxRows = this.mode === TileTableMode.BASIC ? 4 : 1000;
    const rowCount = this.chartData.labels.length < maxRows ? this.chartData.labels.length : maxRows;
    this.getHeaderLabels();
    for (let i = 0; i < rowCount; i++) {
      let row;
      let dataVal;
      if (this.chartData.tooltips) {
        const str = this.chartData.tooltips[i];
        dataVal = str.length > 30 ? `${str.slice(0, 30)}...` : str;
      }
      else {
        dataVal = this.chartData.labels[i];
      }
      if (this.hasDateColumn()) {
        const dateVal = moment.parseZone(dataVal, 'DD.MM.YY').toDate();
        row = { column1: dateVal, column2: this.chartData.datasets[0].data[i] };
      }
      else {
        row = { column1: dataVal, column2: this.chartData.datasets[0].data[i] };
      }
      this.tableData.push(row);
    }
    if (this.mode === TileTableMode.BASIC) {
      this.sortRows();
    }
    this.dataSource = new MatTableDataSource(this.tableData);
  }

  private getHeaderLabels(): void {
    this.headerLabels = this.tileConfigurationService.tableHeaderLabels(this.reportType);
  }

  private sortRows(): void {
    this.tableData.sort((rowA, rowB) => rowB.column2 - rowA.column2);
  }

}
