import { AfterViewInit, Component, ElementRef } from '@angular/core';
import { UserService } from './services/user.service';
import { Event, NavigationCancel, NavigationEnd, NavigationError, NavigationStart, Router } from '@angular/router';

@Component({
  selector: 'gad-app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements AfterViewInit {
  title = 'GoogleAnalyticsDashboard';
  loading = false;

  constructor(
    private elementRef: ElementRef,
    private router: Router,
    private userService: UserService
  ) {
    if (!this.userService.isUserSignedIn) {
      this.router.navigate(['login']);
    }
    this.router.events.subscribe((event: Event) => {
      switch (true) {
        case event instanceof NavigationStart: {
          this.loading = true;
          break;
        }

        case event instanceof NavigationEnd:
        case event instanceof NavigationCancel:
        case event instanceof NavigationError: {
          this.loading = false;
          break;
        }
        default: {
          break;
        }
      }
    });
  }

  ngAfterViewInit(): void {
    // this.elementRef.nativeElement.ownerDocument.body.style.backgroundColor = '#d7d7d7';
  }
}
