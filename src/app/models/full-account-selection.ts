import { ManagementAccount, WebProperty } from '../api/models/managementTypes';

export interface FullAccountSelection {
  account: ManagementAccount;
  property: WebProperty;
}
