export interface TileChartData {
  datasets: { data: any[]; label?: string; }[];
  labels: string[];
  tooltips?: string[];
}
