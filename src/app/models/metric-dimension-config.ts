export interface MetricDimensionConfig {
  metrics: { expression: string }[];
  dimensions: { name: string }[];
}
