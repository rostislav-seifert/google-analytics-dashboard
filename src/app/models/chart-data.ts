export interface ChartData {
  data: any[];
  label?: string;
  backgroundColor?: string | string[];
  borderColor?: string;
  pointBackgroundColor?: string;
  pointBorderColor?: string;
}
