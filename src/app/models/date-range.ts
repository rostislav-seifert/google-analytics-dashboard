import { DateRangeEnum } from '../enumerations/date-range.enum';

export interface DateRange {
  from: string;
  to: string;
  range: DateRangeEnum;
}
