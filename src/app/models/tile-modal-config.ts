import { ReportType } from '../enumerations/report-type.enum';
import { TileChartType } from '../enumerations/tile-chart.enum';
import { DateRange } from './date-range';

export interface TileModalConfig {
  reportType: ReportType;
  tileChartType: TileChartType;
  dateRange: DateRange;
}
