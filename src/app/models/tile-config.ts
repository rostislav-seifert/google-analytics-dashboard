import { ReportType } from '../enumerations/report-type.enum';
import { TileChartType } from '../enumerations/tile-chart.enum';
import { DateRange } from './date-range';

export interface TileConfig {
  index: number;
  order?: number;
  report?: ReportType;
  chart?: TileChartType;
  dateRange?: DateRange;
}
