import { AbstractControl, ValidatorFn } from '@angular/forms';
import { Moment } from 'moment/moment';
import * as moment from 'moment';

export class DateValidators {
  static futureDate(): ValidatorFn {
    return (control: AbstractControl): {[key: string]: any} | null => {
      const date = control.value as Moment;
      const today = moment();
      return date.isAfter(today) ? { dateInFuture: { value: control.value } } : null;
    };
  }

  static afterDate(olderDate: AbstractControl): ValidatorFn {
    return (control: AbstractControl): {[key: string]: any} | null => {
      const date = control.value as Moment;
      return date.isAfter(olderDate.value) ? { afterDate: { value: control.value } } : null;
    };
  }
}
