import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { LoginPageComponent } from './pages/login-page/login-page.component';
import { AccountSelectionPageComponent } from './pages/account-selection-page/account-selection-page.component';
import { DashboardPageComponent } from './pages/dashboard-page/dashboard-page.component';
import { AuthGuard } from './guards/auth.guard';
import { StoreGuard } from './guards/store.guard';
import { environment } from '../environments/environment';

const routes: Routes = [
  { path: 'login', component: LoginPageComponent },
  {
    path: '',
    canActivate: [AuthGuard],
    children: [
      { path: 'account-selection', component: AccountSelectionPageComponent },
      { path: 'dashboard', canActivate: [StoreGuard], component: DashboardPageComponent },

      // logged in, default route
      { path: '', component: environment.mocks.all && environment.mocks.login.all ? AccountSelectionPageComponent : LoginPageComponent }
    ]
  },
  // default route
  { path: '', component: LoginPageComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { relativeLinkResolution: 'legacy' })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
