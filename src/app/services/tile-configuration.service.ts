import { Injectable } from '@angular/core';
import * as moment from 'moment';

import { ReportType } from '../enumerations/report-type.enum';
import { TileChartType } from '../enumerations/tile-chart.enum';
import { MetricDimensionConfig } from '../models/metric-dimension-config';
import { DateRange } from '../models/date-range';
import { DateRangeEnum } from '../enumerations/date-range.enum';
import { ReportsDataService } from './reports-data.service';

// tile configuration for various reports / charts
@Injectable({
  providedIn: 'root'
})
export class TileConfigurationService {

  static readonly DEFAULT_DATE_RANGE: DateRange = {
    from: moment().subtract(30, 'days').format(ReportsDataService.REPORT_DATE_FORMAT),
    to: moment().format(ReportsDataService.REPORT_DATE_FORMAT),
    range: DateRangeEnum.Month
  };

  constructor() { }

  configMetricDimension(report: ReportType): MetricDimensionConfig {
    const config = { metrics: [], dimensions: [] };
    switch (report) {
      case ReportType.traffic:
        config.metrics = [{ name: 'activeUsers' }];
        config.dimensions = [{ name: 'date' }];
        break;
      case ReportType.pageVisits:
        config.metrics = [{ name: 'screenPageViews' }];
        config.dimensions = [{ name: 'pageTitle' }];
        break;
      case ReportType.originCountry:
      case ReportType.originLocalGlobal:
        config.metrics = [{ name: 'activeUsers' }];
        config.dimensions = [{ name: 'country' }];
        break;
      case ReportType.bounceRate:
        config.metrics = [{ name: 'bounceRate' }];
        config.dimensions = [{ name: 'date' }];
        break;
      case ReportType.device:
        config.metrics = [{ name: 'activeUsers' }];
        config.dimensions = [{ name: 'deviceCategory' }];
        break;
      case ReportType.operatingSystem:
        config.metrics = [{ name: 'activeUsers' }];
        config.dimensions = [{ name: 'operatingSystem' }];
        break;
      case ReportType.browser:
        config.metrics = [{ name: 'activeUsers' }];
        config.dimensions = [{ name: 'browser' }];
        break;
      case ReportType.eventCount:
        config.metrics = [{ name: 'eventCount' }];
        config.dimensions = [{ name: 'eventName' }];
        break;
      case ReportType.eventsPerUser:
        config.metrics = [{ name: 'eventCountPerUser' }];
        config.dimensions = [{ name: 'eventName' }];
        break;
      case ReportType.eventsPerSession:
        config.metrics = [{ name: 'eventsPerSession' }];
        config.dimensions = [{ name: 'eventName' }];
        break;
      default:
        break;
    }
    return config;
  }

  recommendedChart(report: ReportType): TileChartType {
    switch (report) {
      case ReportType.traffic:
        return TileChartType.line;
      case ReportType.pageVisits:
        return TileChartType.bar;
      case ReportType.originCountry:
        return TileChartType.bar;
      case ReportType.originLocalGlobal:
        return TileChartType.pie;
      case ReportType.bounceRate:
        return TileChartType.line;
      case ReportType.browser:
      case ReportType.operatingSystem:
      case ReportType.device:
        return TileChartType.pie;
      case ReportType.eventCount:
      case ReportType.eventsPerUser:
      case ReportType.eventsPerSession:
        return TileChartType.bar;
      default:
        break;
    }
  }

  disabledCharts(report: ReportType): TileChartType[] {
    const disabledCharts = [];
    switch (report) {
      case ReportType.traffic:
        disabledCharts.push(TileChartType.pie);
        break;
      case ReportType.pageVisits:
        disabledCharts.push(TileChartType.line, TileChartType.pie);
        break;
      case ReportType.originCountry:
        disabledCharts.push(TileChartType.line);
        break;
      case ReportType.originLocalGlobal:
        disabledCharts.push(TileChartType.line);
        break;
      case ReportType.bounceRate:
        disabledCharts.push(TileChartType.bar, TileChartType.pie);
        break;
      case ReportType.browser:
      case ReportType.device:
      case ReportType.operatingSystem:
        disabledCharts.push(TileChartType.line);
        break;
      case ReportType.eventCount:
      case ReportType.eventsPerUser:
      case ReportType.eventsPerSession:
        disabledCharts.push(TileChartType.line, TileChartType.pie);
        break;
      default:
        break;
    }
    return disabledCharts;
  }

  tableHeaderLabels(reportType: ReportType): { column1: string, column2: string } {
    switch (reportType) {
      case ReportType.traffic:
        return { column1: 'Date', column2: 'Users' };
      case ReportType.pageVisits:
        return { column1: 'Page', column2: 'Visits' };
      case ReportType.originCountry:
        return { column1: 'Country', column2: 'Users' };
      case ReportType.originLocalGlobal:
        return { column1: 'Origin', column2: 'Users' };
      case ReportType.bounceRate:
        return { column1: 'Date', column2: 'Users(%)' };
      case ReportType.device:
        return { column1: 'Device', column2: 'Users(%)' };
      case ReportType.operatingSystem:
        return { column1: 'Operating System', column2: 'Users(%)' };
      case ReportType.browser:
        return { column1: 'Browser', column2: 'Users(%)' };
      case ReportType.eventsPerUser:
        return { column1: 'Action', column2: 'Count' };
      case ReportType.eventCount:
        return { column1: 'Category', column2: 'Count' };
      case ReportType.eventsPerSession:
        return { column1: 'Label', column2: 'Count' };
      default:
        return { column1: '', column2: '' };
    }
  }
}
