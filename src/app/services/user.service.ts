import { Store } from '@ngrx/store';
import { Injectable, NgZone } from '@angular/core';
import { Router } from '@angular/router';
import { GoogleAuthService } from 'ng-gapi/lib/src';
import firebase from '@firebase/app';
import '@firebase/auth';

import * as LoginActions from '../store/actions/login.actions';
import { AppState } from '../store';
import GoogleUser = gapi.auth2.GoogleUser;

// [DOCS] https://developers.google.com/identity/sign-in/web/reference
@Injectable({
  providedIn: 'root'
})
export class UserService {
  static readonly SESSION_KEY_ACCESS_TOKEN: string = 'accessToken';
  static readonly SESSION_KEY_TOKEN_EXP_TIME: string = 'accessTokenExpirationTime';
  redirectUrl = '';
  googleAuthObject: gapi.auth2.GoogleAuth;
  private user: GoogleUser = undefined;

  get isUserSignedIn(): boolean {
    return this.googleAuthObject?.isSignedIn.get();
  }

  constructor(
    private googleAuthService: GoogleAuthService,
    private store: Store<AppState>,
    private router: Router,
    private ngZone: NgZone
  ) { }

  getToken(): string {
    return this.googleAuthObject.currentUser.get().getAuthResponse().access_token;
  }

  signIn(): void {
    this.googleAuthObject.signIn().then(res => this.signInSuccessHandler(res));
  }

  signOut(): void {
    this.googleAuthService.getAuth().subscribe(auth => {
      try {
        auth.signOut();
      }
      catch (e) {
        console.error(e);
      }
      sessionStorage.removeItem(UserService.SESSION_KEY_ACCESS_TOKEN);
      sessionStorage.removeItem(UserService.SESSION_KEY_TOKEN_EXP_TIME);
      this.store.dispatch(new LoginActions.LogOut());
      this.router.navigate(['/login']).then();
    });
  }

  publicSignInSuccessHandler(googleUser: GoogleUser): void {
    // [DOCS] https://firebase.google.com/docs/auth/web/google-signin
    // We need to register an Observer on Firebase Auth to make sure auth is initialized.
    const unsubscribe = firebase.auth().onAuthStateChanged(firebaseUser => {
      unsubscribe();
      // Check if we are already signed-in Firebase with the correct user.
      if (!this.isUserEqual(googleUser, firebaseUser)) {
        // Build Firebase credential with the Google ID token.
        const credential = firebase.auth.GoogleAuthProvider.credential(
          googleUser.getAuthResponse().id_token);

        // Sign in with credential from the Google user.
        firebase.auth().signInWithCredential(credential).catch(error => {
          // Handle Errors here.
          const errorCode = error.code;
          const errorMessage = error.message;
          // The email of the user's account used.
          const email = error.email;
          // The firebase.auth.AuthCredential type that was used.
          const credentialError = error.credential;
          // ...
          throw new Error(`code: ${errorCode}, message: ${errorMessage}, email: ${email}, credential:${credentialError}`);
        });
      } // else - 'User already signed-in Firebase.'
    });

    // Google login
    this.user = googleUser;
    // const authResp = googleUser.getAuthResponse();
    // const token = authResp.access_token;
    // this.store.dispatch(new LoginActions.LogIn(token));
  }

  private async signInSuccessHandler(googleUser: GoogleUser): Promise<void> {
    this.publicSignInSuccessHandler(googleUser);
    await this.ngZone.run(() => {
      this.router.navigate([this.redirectUrl === '' ? '/account-selection' : this.redirectUrl]).then();
    });
  }

  private isUserEqual(googleUser, firebaseUser): boolean {
    if (firebaseUser) {
      const providerData = firebaseUser.providerData;
      for (const providerDataItem of providerData) {
        // providerId = google.com, uid = google user id
        if (providerDataItem.providerId === firebase.auth.GoogleAuthProvider.PROVIDER_ID &&
          providerDataItem.uid === googleUser.getBasicProfile().getId()) {
          // We don't need to reauth the Firebase connection.
          return true;
        }
      }
    }
    return false;
  }
}
