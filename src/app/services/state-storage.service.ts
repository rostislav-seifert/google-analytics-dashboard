import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { first, map, switchMap, take } from 'rxjs/operators';
import { Observable, of } from 'rxjs';
import { AngularFireDatabase } from '@angular/fire/database';
import { Md5 } from 'ts-md5';
import firebase from '@firebase/app';
import '@firebase/auth';

import * as AccountSelectionActions from '../store/actions/account-selection.actions';
import * as LoginActions from '../store/actions/login.actions';
import { InitDashboard } from '../store/actions/dashboard.actions';
import { AppState } from '../store';
import { environment } from '../../environments/environment';
import { TileConfig } from '../models/tile-config';
import { mockTilesConfig } from '../../assets/mocks/MockTilesConfig';

@Injectable({
  providedIn: 'root'
})
export class StateStorageService {
  static readonly SESSION_KEY_STATE: string = 'state';

  constructor(
    private store: Store,
    private db: AngularFireDatabase
  ) { }

  saveStateToSession(): void {
    this.store.select(state => state).pipe(take(1))
      .subscribe((state: AppState) => {
        sessionStorage.setItem(StateStorageService.SESSION_KEY_STATE, JSON.stringify(state));
      });
  }

  async loadStateFromSession(): Promise<boolean> {
    const state = JSON.parse(sessionStorage.getItem(StateStorageService.SESSION_KEY_STATE));
    if (state) {
      // account (view)
      this.store.dispatch(new AccountSelectionActions.Init(state.accountSelection));

      // dashboard
      if (state.dashboard && state.dashboard.tiles && state.dashboard.tiles.length) {
        // dashboard from session
        this.store.dispatch(new InitDashboard(state.dashboard));
      }
      else {
        // dashboard from BE
        const resp = await this.loadStateFromRemoteDB().toPromise();
        if (resp && resp.length !== 0) {
          this.store.dispatch(new InitDashboard({ tiles: resp }));
        }
        else {
          this.store.dispatch(new InitDashboard({ tiles: [] }));
        }
      }

      // login
      this.store.dispatch(new LoginActions.Init(state.logIn.accessToken));
      return true;
    }
    return false;
  }

  // https://github.com/angular/angularfire/blob/master/docs/rtdb/objects.md
  loadStateFromRemoteDB(): Observable<TileConfig[]> {
    if (environment.mocks.all && environment.mocks.fbDb.all) {
      return of(mockTilesConfig);
    }
    return this.store.select(state => state).pipe(
      take(1),
      switchMap((state: AppState) => {
        const key = Md5.hashStr(`${state.accountSelection.accountId} - ${state.accountSelection.id}`) as string;
        const db = this.db.object(`/users/${firebase.auth().currentUser.uid}/${key}`).valueChanges() as Observable<{ tiles: TileConfig[] }>;
        // no BE data returns null => return [] as empty dashboard config (tiles)
        return db.pipe(first(), map(response => response && response.tiles || []));
      })
    );
  }

  saveStateToRemoteDB(): void {
    this.store.select(state => state).pipe(take(1))
      .subscribe((state: AppState) => {
        if (state.dashboard && state.dashboard.tiles) {
          const key = Md5.hashStr(`${state.accountSelection.accountId} - ${state.accountSelection.id}`) as string;
          const itemRef = this.db.object(`/users/${firebase.auth().currentUser.uid}/${key}`);
          itemRef.set(state.dashboard).then();
        }
      });
  }
}
