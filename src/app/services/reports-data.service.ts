import { Injectable } from '@angular/core';
import * as moment from 'moment';

import { TileChartData } from '../models/tile-chart-data';
import { ReportType } from '../enumerations/report-type.enum';
import { ReportResponseRow } from '../api/models/reportResponse/report-response-row';
import { ReportResponse } from '../api/models/reportResponse/report-response';
import { TileMode } from '../enumerations/tile-mode.enum';

// data processing service, modifies report data for display in the tiles
@Injectable({
  providedIn: 'root'
})
export class ReportsDataService {

  static readonly REPORT_DATE_FORMAT = 'YYYY-MM-DD';
  static readonly DISPLAY_DATE_FORMAT = 'DD.MM.YY';
  static readonly TITLE_DATE_FORMAT = 'DD.MM.YYYY';

  constructor() { }

  formatData(responseBody: ReportResponse, tileReportType: ReportType, mode: TileMode): TileChartData {
    // process retrieved data, accordingly to report type
    let tileChartData;
    switch (tileReportType) {
      case ReportType.traffic:
        tileChartData = this.formatTraffic(responseBody, mode);
        break;
      case ReportType.pageVisits:
        tileChartData = this.formatPageVisits(responseBody, mode);
        break;
      case ReportType.originCountry:
        tileChartData = this.formatOriginCountry(responseBody);
        break;
      case ReportType.originLocalGlobal:
        tileChartData = this.formatOriginLocalGlobal(responseBody);
        break;
      case ReportType.bounceRate:
        tileChartData = this.formatBounceRate(responseBody);
        break;
      case ReportType.operatingSystem:
      case ReportType.device:
      case ReportType.browser:
        tileChartData = this.formatPercents(responseBody, tileReportType, mode);
        break;
      case ReportType.eventCount:
      case ReportType.eventsPerUser:
      case ReportType.eventsPerSession:
        tileChartData = this.formatPageVisits(responseBody, mode);
        break;
      default:
        // DEV - for prototyping new reports only
        tileChartData = this.formatRaw(responseBody);
        break;
    }
    return tileChartData;
  }

  hasDateColumn(reportType: ReportType): boolean {
    switch (reportType) {
      case ReportType.bounceRate:
      case ReportType.traffic:
        return true;
      default:
        return false;
    }
  }

  private formatTraffic(responseBody: ReportResponse, mode = TileMode.basic): TileChartData {
    const rows = this.unpackResponseBody(responseBody);
    const data = [];
    let labels = [];
    this.sortDataByDimension(rows);
    rows.forEach(row => {
      data.push(parseInt(row.metricValues[0].value, 10));
      const dateValue = moment(parseInt(row.dimensionValues[0].value, 10), 'YYYYMMDD');
      labels.push(dateValue.format(ReportsDataService.DISPLAY_DATE_FORMAT));
    });
    const usersDataset = { data: data, label: 'Users' };
    if (data.length > 200) {
      const mergedResult = this.handleHighDensity(data, labels, mode);
      usersDataset.data = mergedResult.data;
      labels = mergedResult.labels;
      return { datasets: [usersDataset], labels: labels, tooltips: mergedResult.tooltips };
    }
    return { datasets: [usersDataset], labels: labels };
  }

  private formatPageVisits(responseBody: ReportResponse, mode: TileMode): TileChartData {
    let rows = this.unpackResponseBody(responseBody);
    rows = this.sortData(rows);
    const pageLimit = mode === TileMode.basic ? 10 : 30;
    const maxPageCount = rows.length < pageLimit ? rows.length : pageLimit;
    const data = [];
    const labels = [];
    const tooltips = [];
    for (let i = 0; i < maxPageCount; i++) {
      data.push(parseInt(rows[i].metricValues[0].value, 10));
      labels.push(this.shortenLabel(rows[i].dimensionValues[0].value, 14));
      tooltips.push(rows[i].dimensionValues[0].value);
    }
    const visitsDataset = { data: data, label: 'Visits' };
    return { datasets: [visitsDataset], labels: labels, tooltips: tooltips };
  }

  private formatOriginCountry(responseBody: ReportResponse): TileChartData {
    let rows = this.unpackResponseBody(responseBody);
    rows = this.sortData(rows);
    const maxCountryCount = rows.length < 10 ? rows.length : 10;
    const data = [];
    const labels = [];
    const tooltips = [];
    for (let i = 0; i < maxCountryCount; i++) {
      data.push(parseInt(rows[i].metricValues[0].value, 10));
      labels.push(this.shortenLabel(rows[i].dimensionValues[0].value));
      tooltips.push(rows[i].dimensionValues[0].value);
    }
    const countriesDataset = { data: data, label: 'Countries' };
    return { datasets: [countriesDataset], labels: labels, tooltips: tooltips };
  }

  private formatOriginLocalGlobal(responseBody: ReportResponse): TileChartData {
    const rows = [...this.unpackResponseBody(responseBody)]; // cant directly reference data from cache, due to splice further down
    const data = [];
    const labels = [];

    // find local visitors, save, remove from original data
    const localIndex = rows.findIndex(row => row.dimensionValues[0].value === 'Czechia');
    if (localIndex !== -1) {
      data.push(rows[localIndex].metricValues[0].value);
      rows.splice(localIndex, 1);
    }
    else {
      data.push(0);
    }
    labels.push('Czechia');

    // flatten data to visitor counts only, and reduce to sum
    const rowsFlat = rows.map(row => parseInt(row.metricValues[0].value, 10));
    data.push(rowsFlat.reduce((a, b) => a + b));
    labels.push('Other');

    const countriesDataset = { data: data, label: 'Countries' };
    return { datasets: [countriesDataset], labels: labels };
  }

  private formatBounceRate(responseBody: ReportResponse, mode = TileMode.basic): TileChartData {
    const rows = this.unpackResponseBody(responseBody);
    const data = [];
    let labels = [];
    this.sortDataByDimension(rows);
    rows.forEach(row => {
      data.push(+row.metricValues[0].value * 100);
      const dateValue = moment(parseInt(row.dimensionValues[0].value, 10), 'YYYYMMDD');
      labels.push(dateValue.format(ReportsDataService.DISPLAY_DATE_FORMAT));
    });
    const rawDataset = { data: data, label: 'Bounce rate (%)' };
    if (data.length > 200) {
      const mergedResult = this.handleHighDensity(data, labels, mode);
      rawDataset.data = mergedResult.data;
      labels = mergedResult.labels;
      return { datasets: [rawDataset], labels: labels, tooltips: mergedResult.tooltips };
    }
    return { datasets: [rawDataset], labels: labels };
  }

  private formatPercents(responseBody: ReportResponse, tileReportType: ReportType, mode: TileMode): TileChartData {
    let rows = this.unpackResponseBody(responseBody);
    rows = this.sortData(rows);
    const rowLimit = mode === TileMode.basic ? 10 : 30;
    const maxRowCount = rows.length < rowLimit ? rows.length : rowLimit;
    const percentValues = this.convertToPercents(rows);
    const data = [];
    const labels = [];
    for (let i = 0; i < maxRowCount; i++) {
      data.push(percentValues[i]);
      labels.push(rows[i].dimensionValues[0].value);
    }
    let label = '';
    switch (tileReportType) {
      case ReportType.device:
        label = 'Device';
        break;
      case ReportType.operatingSystem:
        label = 'Operating system';
        break;
      case ReportType.browser:
        label = 'Browser';
        break;
      default:
        break;
    }
    const rawDataset = { data: data, label: label };
    return { datasets: [rawDataset], labels: labels };
  }

  // dev function for new report prototyping
  private formatRaw(responseBody: ReportResponse): TileChartData {
    let rows = this.unpackResponseBody(responseBody);
    rows = this.sortData(rows);
    const maxRowCount = rows.length < 31 ? rows.length : 31;
    const data = [];
    const labels = [];
    for (let i = 0; i < maxRowCount; i++) {
      data.push(parseInt(rows[i].dimensionValues[0].value, 10));
      labels.push(this.shortenLabel(rows[i].dimensionValues[0].value));
    }
    const rawDataset = { data: data, label: 'data' };
    return { datasets: [rawDataset], labels: labels };
  }

  private convertToPercents(rows: ReportResponseRow[]): number[] {
    const rowsFlat = rows.map(row => parseInt(row.metricValues[0].value, 10));
    const sum = rowsFlat.reduce((a, b) => a + b);
    return rowsFlat.map(value => {
      const percent = (value / sum) * 100;
      const val = percent.toFixed(2);
      return parseFloat(val);
    });
  }

  private unpackResponseBody(responseBody: ReportResponse): ReportResponseRow[] {
    return responseBody.reports[0].rows;
  }

  private sortData(rows: ReportResponseRow[]): any {
    rows.sort((a, b) => +b.metricValues[0].value - +a.metricValues[0].value);
    return rows;
  }

  private sortDataByDimension(rows: ReportResponseRow[]): any {
    rows.sort((a, b) => +a.dimensionValues[0].value - +b.dimensionValues[0].value);
    return rows;
  }

  private shortenLabel(label: string, max = 12): string {
    return label.length > max ? `${label.slice(0, max)}...` : label;
  }

  private handleHighDensity(data: any[], labels: string[], mode: TileMode): { data: any[], labels: string[], tooltips: string[] } {
    const basicLimit = 60;
    const fullLimit = 100;
    const limit = mode === TileMode.basic ? basicLimit : fullLimit;
    const step = this.calculateFullModeStep(data.length, limit);

    const mergedData = [];
    const mergedLabels = [];
    const mergedTooltips = [];
    for (let i = 0; i < data.length; i = i + step) {
      const chunk = data.slice(i, i + step);
      mergedData.push(this.average(chunk));
      mergedLabels.push(labels[i]);
      if ((i + step - 1) < data.length) {
        mergedTooltips.push(`${labels[i]} - ${labels[i + step - 1]}`);
      }
      else {
        const remainder = data.length % step;
        mergedTooltips.push(`${labels[i]} - ${labels[i + remainder - 1]}`);
      }
    }
    return { data: mergedData, labels: mergedLabels, tooltips: mergedTooltips };
  }

  private calculateFullModeStep(length, maxPoints): number {
    return Math.ceil(length / maxPoints);
  }

  private average(arr: number[]): number {
    const sum = arr.reduce((a, b) => a + b);
    return Math.round(sum / arr.length);
  }

}
