import { Injectable } from '@angular/core';
import { GoogleApiService, GoogleAuthService } from 'ng-gapi/lib/src';

import { UserService } from './user.service';

@Injectable({
  providedIn: 'root'
})
export class AppInitService {

  constructor(
    private gapiService: GoogleApiService,
    private googleAuthService: GoogleAuthService,
    private userService: UserService
  ) { }

  async init(): Promise<void> {
    // ng-gapi library init
    await this.gapiService.onLoad().toPromise();
    // google auth init
    await this.googleAuthService.getAuth().toPromise().then(async gAuth => { // gapi init inside
      this.userService.googleAuthObject = gAuth;
      const googleUser = this.userService.googleAuthObject.currentUser.get();
      if (googleUser?.isSignedIn()) {
        await this.userService.publicSignInSuccessHandler(googleUser);
      }
    }, () => { });
  }
}
