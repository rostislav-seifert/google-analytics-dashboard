import { Injectable } from '@angular/core';
import { GoogleApiService } from 'ng-gapi/lib/src';
import { first, map, mergeMap, switchMap, tap } from 'rxjs/operators';
import { Observable, of } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Store } from '@ngrx/store';

import { ManagementAccount, WebProperty } from '../api/models/managementTypes';
import { UserService } from './user.service';
import { ReportResponse } from '../api/models/reportResponse/report-response';
import { AppState } from '../store';
import { ReportRequest } from '../api/models/report-request';
import { FullAccountSelection } from '../models/full-account-selection';

@Injectable({
  providedIn: 'root'
})
export class AnalyticsService {

  private accounts: ManagementAccount[] = [];

  constructor(
    private gapiService: GoogleApiService,
    private userService: UserService,
    private http: HttpClient,
    private store: Store<AppState>
  ) { }

  getData(reportRequest: ReportRequest): Observable<ReportResponse> {
    // [DOCS] https://developers.google.com/analytics/devguides/reporting/data/v1/rest/v1beta/properties/batchRunReports
    return this.store.select(state => state.accountSelection.id).pipe(
      first(),
      mergeMap(propertyId => {
        const body = {
          requests: [{
            property: `properties/${propertyId}` ,
            ...reportRequest
          }]
        };
        const options = { headers: this.setHeaders() };
        const requestUrl = `https://analyticsdata.googleapis.com/v1beta/properties/${propertyId}:batchRunReports`;
        return this.http.post<ReportResponse>(requestUrl, body, options);
      }),
      mergeMap(response => {
        // deep copy, due to reference to report response cache
        const responseCopy: ReportResponse = {
          reports: [{
            rows: response.reports[0].rows.slice(),
            rowCount: response.reports[0].rowCount
            // nextPageToken: response.reports[0].nextPageToken,
          }]
        };
        responseCopy.reports[0].rows = response.reports[0].rows.slice();
        return of(response);
      })
    );
  }

  getAvailableAccounts(): Observable<ManagementAccount[]> {
    const url = 'https://analyticsadmin.googleapis.com/v1beta/accountSummaries';
    const options = { headers: this.setHeaders() };
    return this.http.get<any>(url, options).pipe(
      map(response => response?.accountSummaries?.map((account: any) => {
        return {
          id: account.account.split('/')[1], // 'accounts/123456789'
          displayName: account.displayName,
          name: account.name,
          propertySummaries: account?.propertySummaries?.map(property => {
            return {
              displayName: property.displayName,
              parent: property.parent,
              id: property.property.split('/')[1], // 'properties/123456789'
              propertyType: property.propertyType
            };
          })
        };
      })),
      tap(accounts => this.accounts = accounts),
      map(response => response as ManagementAccount[])
    );
  }

  getAccountProperties(accountId: string): WebProperty[] {
    return this.accounts.find(account => account.id === accountId).propertySummaries;
  }

  getFullSelection(accountId: string, propertyId: string): Observable<FullAccountSelection> {
    let foundAccount: ManagementAccount;
    let foundProperty: WebProperty;
    return this.getAvailableAccounts().pipe(
      map(accounts => {
        foundAccount = accounts.find(account => account.id === accountId);
        return this.getAccountProperties(foundAccount?.id);
      }),
      switchMap(properties => {
        foundProperty = properties.find(property => property.id === propertyId);
        return of({
          account: foundAccount,
          property: foundProperty
        });
      })
    );
  }

  private setHeaders(headers?: any): any {
    return { ...headers, authorization: `Bearer ${this.userService.getToken()}` };
  }
}
