import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ToolbarService {
  addTile$;
  clearDashboard$;
  private addTileSource = new Subject<string>();
  private clearDashboardSource = new Subject<string>();

  constructor() {
    this.addTile$ = this.addTileSource.asObservable();
    this.clearDashboard$ = this.clearDashboardSource.asObservable();
  }

  addTileClick(): void {
    this.addTileSource.next();
  }

  clearDashboardClick(): void {
    this.clearDashboardSource.next();
  }
}
