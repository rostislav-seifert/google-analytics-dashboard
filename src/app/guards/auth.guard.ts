import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';

import { UserService } from '../services/user.service';
import { environment } from '../../environments/environment';

// [DOCS] https://angular.io/guide/router-tutorial-toh#milestone-5-route-guards
// 5 - route guards
@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(
    private userService: UserService,
    private router: Router
  ) { }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    return this.checkLogin(state.url);
  }

  private checkLogin(url: string): true | UrlTree {
    if (this.userService.isUserSignedIn || environment.mocks.login.all) {
      return true;
    }

    // store the attempted URL for redirecting
    this.userService.redirectUrl = url === '/' ? '' : url;
    return this.router.parseUrl('/login');
  }
}
