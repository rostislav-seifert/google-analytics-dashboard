import { Injectable } from '@angular/core';
import { ActivatedRoute, ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable, of, throwError } from 'rxjs';
import { Store } from '@ngrx/store';
import { catchError, first, switchMap, take, tap } from 'rxjs/operators';

import { AppState } from '../store';
import { StateStorageService } from '../services/state-storage.service';
import { AccountSelectionState } from '../store/reducers/account-selection.reducer';
import { InitDashboard } from '../store/actions/dashboard.actions';
import { ManagementPropertyStore } from '../api/models/managementTypes';
import * as AccountSelection from '../store/actions/account-selection.actions';
import { AnalyticsService } from '../services/analytics.service';
import { FullAccountSelection } from '../models/full-account-selection';

// https://ultimatecourses.com/blog/preloading-ngrx-store-route-guards
// guard page load without store data -> load store from storage (local/session)
@Injectable({
  providedIn: 'root'
})
export class StoreGuard implements CanActivate {

  constructor(
    private store: Store<AppState>,
    private stateStorageService: StateStorageService,
    private route: ActivatedRoute,
    private analyticsService: AnalyticsService,
    private router: Router
  ) { }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    return this.checkStore(next).pipe(
      switchMap(() => of(true)),
      catchError(() => this.router.navigate(['account-selection']))
    );
  }

  private checkStore(next: ActivatedRouteSnapshot): Observable<AccountSelectionState | any> {
    const routeAccount = next.queryParamMap.get('account');
    const routeProperty = next.queryParamMap.get('property');
    if (!routeAccount || !routeProperty) {
      return throwError('missing query parameter');
    }
    return this.store.select(state => state.accountSelection).pipe(
      first(),
      // check store for account selection (flow through account selection page)
      switchMap(async accountSelection => {
        // no selected account in store
        if (!accountSelection.id.length) {
          // try to load store from storage
          const storeLoaded = await this.stateStorageService.loadStateFromSession();
          if (storeLoaded === false) {
            // no data even in the storage -> flow from bookmarks - handle query params
            return this.dispatchSelectionFromRouteParams(routeAccount, routeProperty).toPromise();
          }
          else {
            // params found in session - do they match query params?
            const newSelection = await this.store.select(state => state.accountSelection).pipe(take(1)).toPromise();
            if (newSelection && newSelection.accountId && newSelection.id && routeAccount && routeProperty) {
              if (newSelection.accountId !== routeAccount || newSelection.id !== routeProperty) {
                return this.dispatchSelectionFromRouteParams(routeAccount, routeProperty).toPromise();
              }
            }
          }
        }
        // selected account found in store
        else {
          // compare link and store data - if same ok, else handle new account (query params)
          if (accountSelection && accountSelection.accountId && accountSelection.id && routeAccount && routeProperty) {
            if (accountSelection.accountId !== routeAccount || accountSelection.id !== routeProperty) {
              return this.dispatchSelectionFromRouteParams(routeAccount, routeProperty).toPromise();
            }
          }
        }
      }),
      switchMap(async response => {
        const tiles = await this.stateStorageService.loadStateFromRemoteDB().toPromise();
        if (tiles && tiles.length !== 0) {
          this.store.dispatch(new InitDashboard({ tiles: tiles }));
        }
        return of(true);
      })
    );
  }

  private dispatchSelectionFromRouteParams(routeAccount: string, routeProperty: string): Observable<FullAccountSelection> {
    return this.analyticsService.getFullSelection(routeAccount, routeProperty).pipe(
      tap(response => {
        const selectedAccount: ManagementPropertyStore = {
          id: routeProperty,
          name: response.property.displayName,
          accountId: routeAccount,
          accountName: response.account.name
        };
        this.store.dispatch(new AccountSelection.SelectAccount(selectedAccount));
        return response;
      })
    );
  }
}
