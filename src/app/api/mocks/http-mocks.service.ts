import { Injectable } from '@angular/core';
import { HttpParams } from '@angular/common/http';

import { MockConfig } from '../models/mock-config';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class HttpMocksService {
  private mockConfig: MockConfig[] = [];
  private mocks = environment.mocks;

  constructor() {
    if (this.mocks.all) {
      if (this.mocks.login.all) {
        this.managementMocks(true);
        this.reportsMocks(true);
      }
      else {
        this.managementMocks(this.mocks.management.all);
        this.reportsMocks(this.mocks.reports.all);
      }
    }
  }

  findRequest(requestUrl: string, requestParams: HttpParams, method: string, requestBody): MockConfig {
    return this.mockConfig.find(conf => {
      const pattern = new RegExp(conf.requestUrl);
      const urlMatches: boolean = pattern.test(requestUrl);
      const paramsMatches = !conf.params || this.paramsMatches(requestParams, conf.params);
      const methodMatches = !conf.method || method === conf.method;
      const bodyMatches = !conf.body || this.bodyMatches(requestBody, conf.body);
      return urlMatches && paramsMatches && methodMatches && bodyMatches;
    });
  }

  private paramsMatches(requestParams: HttpParams, confParams: any): boolean {
    return Array.from(Object.keys(confParams)).every((param: string) => requestParams.get(param) === confParams[param]);
  }

  private bodyMatches(requestBody, confBody: any): boolean {
    const pattern = new RegExp(confBody);
    return pattern.test(JSON.stringify(requestBody));
  }

  private managementMocks(all: boolean): void {
    const managementMocks = this.mocks.management;

    if (all || managementMocks) {
      this.mockConfig.push({
        requestUrl: '.*management\/accounts$',
        jsonUrl: 'management/ACCOUNTS.json'
      });

      this.mockConfig.push({
        requestUrl: '.*management\/accounts\/11119999\/webproperties$',
        jsonUrl: 'management/WEBPROPERTIES-A.json'
      });
      this.mockConfig.push({
        requestUrl: '.*management\/accounts\/22229999\/webproperties$',
        jsonUrl: 'management/WEBPROPERTIES-B.json'
      });
      this.mockConfig.push({
        requestUrl: '.*management\/accounts\/33339999\/webproperties$',
        jsonUrl: 'management/WEBPROPERTIES-C.json'
      });
      this.mockConfig.push({
        requestUrl: '.*management\/accounts\/\\d+\/webproperties$',
        jsonUrl: 'management/WEBPROPERTIES.json'
      });

      this.mockConfig.push({
        requestUrl: '.*management\/accounts\/\\d+\/webproperties\/UA-11119999-9\/profiles$',
        jsonUrl: 'management/PROFILES-A.json'
      });
      this.mockConfig.push({
        requestUrl: '.*management\/accounts\/\\d+\/webproperties\/UA-22229999-9\/profiles$',
        jsonUrl: 'management/PROFILES-B.json'
      });
      this.mockConfig.push({
        requestUrl: '.*management\/accounts\/\\d+\/webproperties\/UA-33338888-8/profiles$',
        jsonUrl: 'management/PROFILES-C.json'
      });
      this.mockConfig.push({
        requestUrl: '.*management\/accounts\/\\d+\/webproperties\/UA-33339999-9/profiles$',
        jsonUrl: 'management/PROFILES-D.json'
      });
      this.mockConfig.push({
        requestUrl: '.*management\/accounts\/\\d+\/webproperties\/[A-Z\\d-]+\/profiles$',
        jsonUrl: 'management/PROFILES.json'
      });
    }
  }

  private reportsMocks(all: boolean): void {
    const reportsMocks = this.mocks.reports;

    if (all || reportsMocks) {
      this.mockConfig.push({
        requestUrl: '.*\/reports:batchGet',
        jsonUrl: 'reports/USERS-DATE.json',
        method: 'POST',
        body: '.*ga:users.*ga:date.*'
      });
      this.mockConfig.push({
        requestUrl: '.*\/reports:batchGet',
        jsonUrl: 'reports/ENTRANCES-PAGETITLE.json',
        method: 'POST',
        body: '.*ga:entrances.*ga:pageTitle.*'
      });
      this.mockConfig.push({
        requestUrl: '.*\/reports:batchGet',
        jsonUrl: 'reports/USERS-COUNTRY.json',
        method: 'POST',
        body: '.*ga:users.*ga:country.*'
      });
      this.mockConfig.push({
        requestUrl: '.*\/reports:batchGet',
        jsonUrl: 'reports/BOUNCERATE-DATE.json',
        method: 'POST',
        body: '.*ga:bounceRate.*ga:date.*'
      });
      this.mockConfig.push({
        requestUrl: '.*\/reports:batchGet',
        jsonUrl: 'reports/USERS-DEVICE.json',
        method: 'POST',
        body: '.*ga:users.*ga:deviceCategory.*'
      });
      this.mockConfig.push({
        requestUrl: '.*\/reports:batchGet',
        jsonUrl: 'reports/USERS-OS.json',
        method: 'POST',
        body: '.*ga:users.*ga:operatingSystem.*'
      });
      this.mockConfig.push({
        requestUrl: '.*\/reports:batchGet',
        jsonUrl: 'reports/USERS-BROWSER.json',
        method: 'POST',
        body: '.*ga:users.*ga:browser.*'
      });
      this.mockConfig.push({
        requestUrl: '.*\/reports:batchGet',
        jsonUrl: 'reports/GTM/CATEGORY.json',
        method: 'POST',
        body: '.*ga:totalEvents.*eventCategory.*'
      });
      this.mockConfig.push({
        requestUrl: '.*\/reports:batchGet',
        jsonUrl: 'reports/GTM/ACTION.json',
        method: 'POST',
        body: '.*ga:totalEvents.*ga:eventAction.*'
      });
      this.mockConfig.push({
        requestUrl: '.*\/reports:batchGet',
        jsonUrl: 'reports/GTM/LABEL.json',
        method: 'POST',
        body: '.*ga:totalEvents.*ga:eventLabel.*'
      });
    }
  }
}
