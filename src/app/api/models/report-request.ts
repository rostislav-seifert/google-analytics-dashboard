export interface ReportRequest {
  viewId?: string;
  dateRanges?: any[];
  metrics?: any[];
  dimensions?: any[];
  pageToken?: string;
}
