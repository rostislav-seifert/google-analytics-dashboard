export interface ReportResponseRow {
  dimensionValues: { value: string }[];
  metricValues: { value: string }[];
}
