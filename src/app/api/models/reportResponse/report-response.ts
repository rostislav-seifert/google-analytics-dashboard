import { ReportResponseRow } from './report-response-row';

export interface ReportResponse {
  reports: {
    rowCount: number;
    rows: ReportResponseRow[];
  }[];
}
