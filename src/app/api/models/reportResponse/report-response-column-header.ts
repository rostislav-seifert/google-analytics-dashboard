export interface ReportResponseColumnHeader {
  dimensions: string[];
  metricHeader: {
    metricHeaderEntries: { name: string; type: string; }[]
  };
}
