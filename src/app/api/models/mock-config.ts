export interface MockConfig {
  requestUrl: string;
  jsonUrl?: string;
  params?: { [s: string]: string; };
  method?: string;
  body?: string;
}
