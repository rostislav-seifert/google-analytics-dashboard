export interface ManagementAccount {
  id: string; // account: 'account/123456789', 'accounts/' stripped
  displayName: string;
  name: string;
  propertySummaries: WebProperty[];
}

export interface WebProperty {
  displayName: string;
  parent: string;
  id: string; // property: 'properties/123456789, 'properties/' stripped
  propertyType: string;
}

export interface ManagementPropertyStore {
  'id': string;
  'name': string;
  'accountId': string;
  'accountName': string;
}
