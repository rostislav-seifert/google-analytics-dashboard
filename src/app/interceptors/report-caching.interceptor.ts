import { Injectable } from '@angular/core';
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { share, tap } from 'rxjs/operators';
import { Md5 } from 'ts-md5';

// cache requests with interceptor - https://medium.com/@vikeshm/data-caching-angular-http-interceptor-2d87f95e2340
@Injectable()
export class ReportCachingInterceptor implements HttpInterceptor {
  private cachedData = new Map<string, any>();

  constructor() {}

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    // skip
    if (request.method !== 'POST' || !this.testUrl(request.url)) {
      return next.handle(request);
    }

    // reset
    if (request.headers.get('reset-cache')) {
      this.cachedData.delete(request.urlWithParams);
    }

    // read cache
    const pastResponse = this.cachedData.get(this.requestBodyHash(request));
    if (pastResponse) {
      return (pastResponse instanceof Observable) ? pastResponse : of(pastResponse.clone());
    }

    // cache response
    const requestHandle = next.handle(request).pipe(
      tap(stateEvent => {
        if (stateEvent instanceof HttpResponse) {
          this.cachedData.set(this.requestBodyHash(request), stateEvent.clone());
        }
      }),
      share()
    );
    // cache request (in case of parallel requests)
    this.cachedData.set(this.requestBodyHash(request), requestHandle);
    return requestHandle;
  }

  private testUrl(url: string): boolean {
    const pattern = new RegExp('.*:batchRunReports');
    return pattern.test(url);
  }

  private requestBodyHash(request: HttpRequest<unknown>): string {
    return Md5.hashStr(JSON.stringify(request.body)) as string;
  }
}
