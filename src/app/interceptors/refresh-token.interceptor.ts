import { Injectable } from '@angular/core';
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { filter, finalize, switchMap, take } from 'rxjs/operators';

import { UserService } from '../services/user.service';

// [NOTE] ! CURRENTLY NOT IN USE !

// https://medium.com/angular-in-depth/top-10-ways-to-use-interceptors-in-angular-db450f8a62d6
// 1. Authentication - Refresh Token
@Injectable()
export class RefreshTokenInterceptor implements HttpInterceptor {
  private AUTH_HEADER = 'Authorization';
  private refreshTokenInProgress = false;
  private refreshTokenSubject: BehaviorSubject<any> = new BehaviorSubject<any>(null);

  constructor(
    private userService: UserService
  ) {}

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    const gapiUrls = [
      'www.googleapis.com',
      'analyticsreporting.googleapis.com'
    ];
    // is request to google API
    gapiUrls.some(url => {
      const pattern = new RegExp(url);
      return pattern.test(request.url);
    });

    if (this.refreshTokenInProgress) {
      // If refreshTokenInProgress is true, we will wait until refreshTokenSubject has a non-null value
      // which means the new token is ready and we can retry the request again
      return this.refreshTokenSubject.pipe(
        filter(result => result !== null),
        take(1),
        switchMap(() => next.handle(this.addNewToken(request)))
      );
    }
    else {
      this.refreshTokenInProgress = true;

      // Set the refreshTokenSubject to null so that subsequent API calls will wait until the new token has been retrieved
      this.refreshTokenSubject.next(null);

      return this.refreshAccessToken().pipe(
        switchMap((success: boolean) => {
          this.refreshTokenSubject.next(success);
          return next.handle(this.addNewToken(request));
        }),
        // When the call to refreshToken completes we reset the refreshTokenInProgress to false
        // for the next time the token needs to be refreshed
        finalize(() => this.refreshTokenInProgress = false)
      );
    }
  }

  private refreshAccessToken(): Observable<any> {
    // return from(this.userService.refreshAccesToken());
    return of();
  }

  private addNewToken(request: HttpRequest<any>): HttpRequest<any> {
    return request.clone({
      headers: request.headers.set(this.AUTH_HEADER, `Bearer ${this.userService.getToken()}`)
    });
  }
}
