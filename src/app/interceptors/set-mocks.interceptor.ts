import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs';

import { HttpMocksService } from '../api/mocks/http-mocks.service';

@Injectable()
export class SetMocksInterceptor implements HttpInterceptor {

  constructor(
    private mocks: HttpMocksService
  ) {}

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    const mockResponse = this.mocks.findRequest(request.url, request.params, request.method, request.body);

    if (mockResponse && mockResponse.jsonUrl) {
      // cant mock POST requests this way, so make them GET instead - works for now
      if (mockResponse.method === 'POST') {
        request = request.clone({ method: 'GET' });
      }
      request = request.clone({ url: `assets/mocks/${mockResponse.jsonUrl}` });
    }
    return next.handle(request);
  }
}
