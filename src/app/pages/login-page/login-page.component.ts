import { Component, OnInit } from '@angular/core';

import { UserService } from '../../services/user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'gad-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.scss']
})
export class LoginPageComponent implements OnInit {
  loggedIn = false;

  constructor(
    private userService: UserService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.loggedIn = this.isLoggedIn();
    if (this.loggedIn) {
    }
  }

  signIn(): void {
    this.userService.signIn();
  }

  signOut(): void {
    this.userService.signOut();
    this.loggedIn = false;
  }

  routeToDashboard(): void {
    this.router.navigate(['/account-selection']).then();
  }

  isLoggedIn(): boolean {
    return this.userService.isUserSignedIn;
  }

}
