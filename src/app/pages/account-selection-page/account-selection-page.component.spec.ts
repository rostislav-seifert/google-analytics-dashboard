import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { AccountSelectionPageComponent } from './account-selection-page.component';

describe('AccountSelectionPageComponent', () => {
  let component: AccountSelectionPageComponent;
  let fixture: ComponentFixture<AccountSelectionPageComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ AccountSelectionPageComponent ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountSelectionPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
