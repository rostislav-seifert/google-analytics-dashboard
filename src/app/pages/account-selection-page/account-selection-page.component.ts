import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { first } from 'rxjs/operators';

import { ManagementAccount, ManagementPropertyStore, WebProperty } from '../../api/models/managementTypes';
import { AnalyticsService } from '../../services/analytics.service';
import { AppState } from '../../store';
import * as AccountSelection from '../../store/actions/account-selection.actions';
import { InitDashboard } from '../../store/actions/dashboard.actions';

@Component({
  selector: 'gad-account-selection-page',
  templateUrl: './account-selection-page.component.html',
  styleUrls: ['./account-selection-page.component.scss']
})
export class AccountSelectionPageComponent implements OnInit {

  // radio-groups
  accounts: ManagementAccount[];
  properties: WebProperty[];
  selectedAccountId: string;
  selectedPropertyId: string;

  noAccounts = false;

  constructor(
    private analyticsService: AnalyticsService,
    private router: Router,
    private store: Store<AppState>
  ) { }

  async ngOnInit(): Promise<void> {
    this.accounts = [];
    const returnedAccounts = await this.analyticsService.getAvailableAccounts().toPromise();
    if (returnedAccounts.length === 0) {
      this.noAccounts = true;
    }
    this.accounts = returnedAccounts;
  }

  async onConfirm(): Promise<void> {
    if (this.selectedPropertyId && this.selectedPropertyId !== '') {
      // save selected account to store
      const property: WebProperty = this.properties.find(item => item.id === this.selectedPropertyId);
      const account: ManagementAccount = this.accounts.find(item => item.id === this.selectedAccountId);
      const selectedAccount: ManagementPropertyStore = {
        id: property.id,
        name: property.displayName,
        accountId: account.id,
        accountName: account.name
      };
      this.store.select((state: AppState) => state.accountSelection).pipe(first()).subscribe(async accountSelectionState => {
        if (accountSelectionState.accountId !== selectedAccount.accountId || accountSelectionState.id !== selectedAccount.id) {
          // switched accounts - clear dashboard state in store and localStorage
          this.store.dispatch(new InitDashboard({ tiles: [] }));
        }

        this.store.dispatch(new AccountSelection.SelectAccount(selectedAccount));
        // route to dashboard
        await this.router.navigate(
          ['/dashboard'],
          { queryParams: { account: this.selectedAccountId, property: this.selectedPropertyId } }
          ).then();
      });
    }
  }

  async onAccountSelect(accountId: string): Promise<void> {
    if (accountId === null) {
      this.selectedAccountId = null;
      this.resetProperties();
    }
    else {
      if (this.selectedAccountId !== accountId) {
        this.selectedAccountId = accountId;
        this.resetProperties();
        // scroll to next card
        this.scrollTo('select-property');
        // get data
        this.properties = [];
        this.properties = this.analyticsService.getAccountProperties(this.selectedAccountId);
      }
    }
  }

  async onPropertySelect(propertyId: string): Promise<void> {
    if (propertyId === null) {
      this.selectedPropertyId = null;
    }
    else {
      if (this.selectedPropertyId !== propertyId) {
        this.selectedPropertyId = propertyId;
        // scroll to confirm button (on mobile landscape)
        this.scrollTo('select-confirm');
      }
    }
  }

  private scrollTo(targetElement: string): void {
    const element = document.getElementById(targetElement);
    const headerOffset = 56; // toolbar height
    const elementPosition = element.getBoundingClientRect().top;
    const offsetPosition = elementPosition - headerOffset;

    window.scrollBy({
      top: offsetPosition,
      behavior: 'smooth'
    });
  }

  private resetProperties(): void {
    this.properties = null;
    this.selectedPropertyId = null;
  }
}
