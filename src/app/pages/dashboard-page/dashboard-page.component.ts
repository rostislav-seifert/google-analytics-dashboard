import { Component, ComponentFactoryResolver, OnDestroy, OnInit, ViewChild, ViewContainerRef } from '@angular/core';
import { first } from 'rxjs/operators';
import { Store } from '@ngrx/store';
import { Options, SortableEvent } from 'sortablejs';
import { MatDialog } from '@angular/material/dialog';
import { Subscription } from 'rxjs';

import { AnalyticsService } from '../../services/analytics.service';
import { AppState } from '../../store';
import { TileComponent } from '../../components/tile/tile.component';
import { TileDirective } from '../../directives/tile.directive';
import { TileItem } from '../../components/tile/tile-item';
import { AddTile, ClearDashboard, MoveTile } from '../../store/actions/dashboard.actions';
import { TileConfig } from '../../models/tile-config';
import { ReportType } from '../../enumerations/report-type.enum';
import { TileChartType } from '../../enumerations/tile-chart.enum';
import { TileConfigurationService } from '../../services/tile-configuration.service';
import { ClearDashboardModalComponent } from '../../components/clear-dashboard-modal/clear-dashboard-modal.component';
import { ToolbarService } from '../../services/toolbar.service';
import { ManagementPropertyStore } from '../../api/models/managementTypes';

// https://angular.io/guide/dynamic-component-loader
@Component({
  selector: 'gad-dashboard-page',
  templateUrl: './dashboard-page.component.html',
  styleUrls: ['./dashboard-page.component.scss']
})
export class DashboardPageComponent implements OnInit, OnDestroy {
  selectedProperty: ManagementPropertyStore;
  viewContainerRef: ViewContainerRef;
  tileIndexer = 0;
  // [DOCS] Options https://github.com/SortableJS/sortablejs
  sortableOptions: Options = {
    disabled: this.isTouchDevice(),
    ghostClass: 'tile-placeholder',
    animation: 250,
    onUpdate: event => this.onTileMoved(event)
  };
  clearDashboardSubscription: Subscription;
  addTileSubscription: Subscription;

  @ViewChild(TileDirective, { static: true })
  tileHost: TileDirective;

  constructor(
    private store: Store<AppState>,
    private analyticsService: AnalyticsService,
    private componentFactoryResolver: ComponentFactoryResolver,
    private dialog: MatDialog,
    private toolbarService: ToolbarService
  ) { }

  ngOnInit(): void {
    this.store.select(state => state)
      .pipe(first())
      .subscribe(async state => {
        this.selectedProperty = state.accountSelection;
        this.viewContainerRef = this.tileHost.viewContainerRef;
        const tiles = state.dashboard.tiles;

        // load tiles configs if there are any
        if (tiles.length) {
          // find tile with highest index
          const lastTile = tiles.reduce((prev, currentMax) => (prev.index > currentMax.index) ? prev : currentMax);
          // set indexer for the next new tile
          this.tileIndexer = lastTile.index + 1;

          // recreate all previous tiles
          this.initTiles(tiles);
        }
      });

    this.addTileSubscription = this.toolbarService.addTile$.subscribe(() => this.addTile());
    this.clearDashboardSubscription = this.toolbarService.clearDashboard$.subscribe(() => this.removeTiles());
  }

  ngOnDestroy(): void {
    this.addTileSubscription.unsubscribe();
    this.clearDashboardSubscription.unsubscribe();
  }

  onTileMoved(event: SortableEvent): void {
    this.store.dispatch(new MoveTile({ oldOrder: event.oldDraggableIndex, newOrder: event.newDraggableIndex }));
  }

  addTile(init: TileConfig = null): void {
    const tileConfig: TileConfig = { index: init && init.index !== null ? init.index : this.tileIndexer };
    if (init) {
      // read existing tile config (older)
      tileConfig.report = init.report || ReportType.traffic;
      tileConfig.chart = init.chart || TileChartType.line;

      if (init.dateRange) {
        tileConfig.dateRange = init.dateRange;
      }
    }
    else {
      // create new tile
      const config: TileConfig = {
        index: this.tileIndexer,
        report: ReportType.traffic,
        chart: TileChartType.line,
        dateRange: TileConfigurationService.DEFAULT_DATE_RANGE
      };
      this.store.dispatch(new AddTile(config));
      this.tileIndexer++;
    }
    const newTile = new TileItem(TileComponent, tileConfig);
    const componentFactory = this.componentFactoryResolver.resolveComponentFactory(newTile.component);

    const tileComponentRef = this.viewContainerRef.createComponent<TileComponent>(componentFactory);
    tileComponentRef.instance._ref = tileComponentRef;
    tileComponentRef.instance.data = newTile.data;
  }

  removeTiles(): void {
    const dialogRef = this.dialog.open(ClearDashboardModalComponent, { width: '250px' });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.store.dispatch(new ClearDashboard());
        this.viewContainerRef.clear();
      }
    });
  }

  private initTiles(tiles: TileConfig[]): void {
    const orderedTiles = this.sortTiles(tiles);
    orderedTiles.forEach(tile => this.addTile(tile));
  }

  private sortTiles(tiles: TileConfig[]): TileConfig[] {
    if (tiles.length) {
      return [...tiles].sort((a, b) => a.order - b.order);
    }
    else {
      return tiles;
    }
  }

  private isTouchDevice(): boolean {
    return (('ontouchstart' in window) ||
      (navigator.maxTouchPoints > 0) ||
      (navigator.msMaxTouchPoints > 0));
  }

}
