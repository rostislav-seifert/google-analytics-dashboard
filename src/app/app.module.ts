import { BrowserModule } from '@angular/platform-browser';
import { APP_INITIALIZER, NgModule } from '@angular/core';
import { StoreModule } from '@ngrx/store';
import { GoogleApiModule, NG_GAPI_CONFIG, NgGapiClientConfig } from 'ng-gapi/lib/src';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { ChartsModule } from 'ng2-charts';
import { MatCardModule } from '@angular/material/card';
import { MatSelectModule } from '@angular/material/select';
import { MatButtonModule } from '@angular/material/button';
import { EffectsModule } from '@ngrx/effects';
import { MatTableModule } from '@angular/material/table';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatInputModule } from '@angular/material/input';
import { MatNativeDateModule } from '@angular/material/core';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatIconModule } from '@angular/material/icon';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatRadioModule } from '@angular/material/radio';
import { MatListModule } from '@angular/material/list';
import { AngularFireModule } from '@angular/fire';
import { MatDialogModule } from '@angular/material/dialog';
import { SortablejsModule } from 'ngx-sortablejs';
import { MatSortModule } from '@angular/material/sort';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatMenuModule } from '@angular/material/menu';

import { SetMocksInterceptor } from './interceptors/set-mocks.interceptor';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginPageComponent } from './pages/login-page/login-page.component';
import { AccountSelectionPageComponent } from './pages/account-selection-page/account-selection-page.component';
import { environment } from '../environments/environment';
import { TileComponent } from './components/tile/tile.component';
import { loginReducer } from './store/reducers/login.reducer';
import { accountSelectionReducer } from './store/reducers/account-selection.reducer';
import { ChartTypeSelectComponent } from './components/chart-type-select/chart-type-select.component';
import { DashboardPageComponent } from './pages/dashboard-page/dashboard-page.component';
import { TileDirective } from './directives/tile.directive';
import { ReportSelectComponent } from './components/report-select/report-select.component';
import { StateStorageEffects } from './store/effects/state-storage.effects';
import { dashboardReducer } from './store/reducers/dashboard.reducer';
import { TileTableComponent } from './components/tile-table/tile-table.component';
import { ReportCachingInterceptor } from './interceptors/report-caching.interceptor';
import { AppInitService } from './services/app-init.service';
import { AccountSelectionCardComponent } from './components/account-selection-card/account-selection-card.component';
import { TileModalComponent } from './components/tile-modal/tile-modal.component';
import { DateRangeToggleComponent } from './components/date-range-toggle/date-range-toggle.component';
import { DateRangeComponent } from './components/date-range/date-range.component';
import { AreYouSureModalComponent } from './components/are-you-sure-modal/are-you-sure-modal.component';
import { ToolbarContentComponent } from './components/toolbar-content/toolbar-content.component';
import { ClearDashboardModalComponent } from './components/clear-dashboard-modal/clear-dashboard-modal.component';
import { DashboardHeaderComponent } from './components/dashboard-header/dashboard-header.component';
import { DashboardWidthDirective } from './directives/dashboard-width.directive';

const gapiClientConfig: NgGapiClientConfig = {
  // OAuth2 Client ID
  client_id: '285505730828-eamn05dnnhtk1pfph316lh2rtso6npqu.apps.googleusercontent.com',
  discoveryDocs: ['https://analyticsdata.googleapis.com/$discovery/rest?version=v1beta'],
  scope: [
    'https://www.googleapis.com/auth/analytics.readonly'
    // 'https://www.googleapis.com/auth/analytics'
  ].join(' ')
};

@NgModule({
  declarations: [
    AppComponent,
    LoginPageComponent,
    AccountSelectionPageComponent,
    TileComponent,
    ChartTypeSelectComponent,
    DashboardPageComponent,
    TileDirective,
    ReportSelectComponent,
    TileTableComponent,
    AccountSelectionCardComponent,
    TileModalComponent,
    DateRangeToggleComponent,
    DateRangeComponent,
    AreYouSureModalComponent,
    ToolbarContentComponent,
    ClearDashboardModalComponent,
    DashboardHeaderComponent,
    DashboardWidthDirective
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ChartsModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    StoreModule.forRoot({
      logIn: loginReducer,
      accountSelection: accountSelectionReducer,
      dashboard: dashboardReducer
    }),
    StoreDevtoolsModule.instrument({
      maxAge: 25, // Retains last 25 states
      logOnly: environment.production // Restrict extension to log-only mode
    }),
    GoogleApiModule.forRoot({
      provide: NG_GAPI_CONFIG,
      useValue: gapiClientConfig
    }),
    AngularFireModule.initializeApp(environment.firebase),
    BrowserAnimationsModule,
    MatSelectModule,
    MatButtonModule,
    MatProgressSpinnerModule,
    MatCardModule,
    EffectsModule.forRoot([StateStorageEffects]),
    MatTableModule,
    MatDatepickerModule,
    MatInputModule,
    MatNativeDateModule,
    MatButtonToggleModule,
    MatIconModule,
    MatTooltipModule,
    MatRadioModule,
    MatListModule,
    MatDialogModule,
    SortablejsModule,
    MatSortModule,
    MatToolbarModule,
    MatMenuModule
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: SetMocksInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ReportCachingInterceptor, multi: true },
    { provide: APP_INITIALIZER, useFactory: performInit, multi: true, deps: [AppInitService] }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

export function performInit(initService: AppInitService): () => void {
  return () => initService.init();
}
