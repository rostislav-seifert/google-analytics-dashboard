import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { catchError, map, mergeMap, tap } from 'rxjs/operators';
import { EMPTY, of } from 'rxjs';

import { StateStorageService } from '../../services/state-storage.service';
import { AccountSelectionActionTypes } from '../actions/account-selection.actions';
import { DashboardActionTypes } from '../actions/dashboard.actions';
import { environment } from '../../../environments/environment';
// [DOCS] https://ngrx.io/guide/effects
@Injectable()
export class StateStorageEffects {

  saveState$ = createEffect(() => this.actions$.pipe(
    ofType(
      AccountSelectionActionTypes.SELECT_ACCOUNT,
      DashboardActionTypes.ADD_TILE,
      DashboardActionTypes.MOVE_TILE,
      DashboardActionTypes.DELETE_TILE,
      DashboardActionTypes.CLEAR_DASHBOARD,
      DashboardActionTypes.SET_TILE_CHART,
      DashboardActionTypes.SET_TILE_REPORT,
      DashboardActionTypes.SET_TILE_DATE_RANGE
    ),
    tap(action => {
      if (action.type !== AccountSelectionActionTypes.SELECT_ACCOUNT && !(environment.mocks.all && environment.mocks.fbDb)) {
        this.stateStorageService.saveStateToRemoteDB();
      }
    }),
    mergeMap(() => of(this.stateStorageService.saveStateToSession())
      .pipe(
        map(() => {
          return ({ type: '[Account Select] Save', payload: null });
        }),
        catchError(() => EMPTY)
    ))
  ));

  constructor(
    private actions$: Actions,
    private stateStorageService: StateStorageService
  ) {}

}
