import { Action } from '@ngrx/store';

export const INIT = '[User] Init';
export const LOG_IN = '[User] Log In';
export const LOG_OUT = '[User] Log Out';

export class Init implements Action {
  readonly type = INIT;

  constructor(public payload: string) {
  }
}

export class LogIn implements Action {
  readonly type = LOG_IN;

  constructor(public payload: string) {
  }
}

export class LogOut implements Action {
  readonly type = LOG_OUT;

  constructor() {
  }
}

export type LoginActions =
  Init
  | LogIn
  | LogOut;
