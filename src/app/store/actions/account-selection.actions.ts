import { Action } from '@ngrx/store';

import { ManagementPropertyStore } from '../../api/models/managementTypes';

export enum AccountSelectionActionTypes {
  INIT = '[Account Select] Init',
  SELECT_ACCOUNT = '[Account Select] Select',
  SAVE = '[Account Select] Save'
}

export class Init implements Action {
  readonly type = AccountSelectionActionTypes.INIT;

  constructor(public payload: ManagementPropertyStore) {
  }
}

export class SelectAccount implements Action {
  readonly type = AccountSelectionActionTypes.SELECT_ACCOUNT;

  constructor(public payload: ManagementPropertyStore) {
  }
}

export class Save implements Action {
  readonly type = AccountSelectionActionTypes.SAVE;

  constructor() {
  }
}

export type AccountSelectionActions =
  Init
  | SelectAccount
  | Save;
