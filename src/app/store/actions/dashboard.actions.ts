import { Action } from '@ngrx/store';

import { ReportType } from '../../enumerations/report-type.enum';
import { TileChartType } from '../../enumerations/tile-chart.enum';
import { DateRange } from '../../models/date-range';
import { TileConfig } from '../../models/tile-config';

export enum DashboardActionTypes {
  INIT_DASHBOARD = '[Dashboard] Init',
  ADD_TILE = '[Dashboard] Add Tile',
  SET_TILE_REPORT = '[Dashboard] Set Tile Report',
  SET_TILE_CHART = '[Dashboard] Set Tile Chart',
  SET_TILE_DATE_RANGE = '[Dashboard] Set Tile Date Range',
  MOVE_TILE = '[Dashboard] Move Tile',
  DELETE_TILE = '[Dashboard] Delete Tile',
  CLEAR_DASHBOARD = '[Dashboard] Clear',
  SAVE = '[DASHBOARD] Save'
}

export class InitDashboard implements Action {
  readonly type = DashboardActionTypes.INIT_DASHBOARD;

  constructor(public payload: any) { }
}
export class AddTile implements Action {
  readonly type = DashboardActionTypes.ADD_TILE;

  constructor(public payload: TileConfig) { }
}
export class SetTileReport implements Action {
  readonly type = DashboardActionTypes.SET_TILE_REPORT;

  constructor(public payload: { index: number, reportType: ReportType }) { }
}
export class SetTileChart implements Action {
  readonly type = DashboardActionTypes.SET_TILE_CHART;

  constructor(public payload: { index: number, chartType: TileChartType }) { }
}
export class SetTileDateRange implements Action {
  readonly type = DashboardActionTypes.SET_TILE_DATE_RANGE;

  constructor(public payload: { index: number, date: DateRange }) { }
}
export class MoveTile implements Action {
  readonly type = DashboardActionTypes.MOVE_TILE;

  constructor(public payload: { oldOrder: number, newOrder: number }) { }
}
export class DeleteTile implements Action {
  readonly type = DashboardActionTypes.DELETE_TILE;

  constructor(public payload: number) { }
}
export class ClearDashboard implements Action {
  readonly type = DashboardActionTypes.CLEAR_DASHBOARD;

  constructor() { }
}
export class Save implements Action {
  readonly type = DashboardActionTypes.SAVE;

  constructor() {}
}

export type DashboardActions =
  InitDashboard
  | AddTile
  | SetTileReport
  | SetTileChart
  | SetTileDateRange
  | MoveTile
  | DeleteTile
  | ClearDashboard
  | Save;
