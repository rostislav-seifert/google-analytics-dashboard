import * as LoginActions from '../actions/login.actions';

export interface LoginState {
  loggedIn: boolean;
  accessToken: string;
}

const initialState: LoginState = {
  loggedIn: false,
  accessToken: ''
};

export function loginReducer(state: LoginState = initialState, action: LoginActions.LoginActions): LoginState {
  switch (action.type) {
    case LoginActions.INIT:
      return {
        ...state,
        loggedIn: true,
        accessToken: action.payload
      };
    case LoginActions.LOG_IN:
      return {
        ...state,
        loggedIn: true,
        accessToken: action.payload
      };
    case LoginActions.LOG_OUT:
      return {
        ...state,
        loggedIn: false,
        accessToken: ''
      };
    default:
      return state;
  }
}
