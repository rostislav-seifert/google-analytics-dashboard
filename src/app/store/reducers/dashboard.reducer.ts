import { DashboardActions, DashboardActionTypes } from '../actions/dashboard.actions';
import { TileConfig } from '../../models/tile-config';

export interface DashboardState {
  tiles: TileConfig[];
}

export const initialState: DashboardState = {
  tiles: []
};

export function dashboardReducer(state = initialState, action: DashboardActions): DashboardState {
  switch (action.type) {
    case DashboardActionTypes.INIT_DASHBOARD:
      return {
        ...state,
        tiles: action.payload.tiles
      };
    case DashboardActionTypes.ADD_TILE:
      return {
        ...state,
        tiles: [...state.tiles, { ...action.payload, order: state.tiles.length }]
      };
    case DashboardActionTypes.MOVE_TILE:
      const stateTiles = [...state.tiles];
      // TODO refactor, 27, 32
      const mtile = stateTiles.splice(state.tiles.indexOf(state.tiles.find(tile => tile.order === action.payload.oldOrder)), 1)[0];
      const movedTile = { ...mtile };
      movedTile.order = action.payload.newOrder;

      const tiles = [];
      if (action.payload.oldOrder < action.payload.newOrder) {
        stateTiles.forEach(tile => {
          if (tile.order < action.payload.oldOrder || tile.order > action.payload.newOrder) {
            tiles.push(tile);
          }
          else {
            const newTile = { ...tile };
            newTile.order--;
            tiles.push(newTile);
          }
        });
      }
      else {
        stateTiles.forEach(tile => {
          if (tile.order > action.payload.oldOrder || tile.order < action.payload.newOrder) {
            tiles.push(tile);
          }
          else {
            const newTile = { ...tile };
            newTile.order++;
            tiles.push(newTile);
          }
        });
      }

      return {
        ...state,
        tiles: [...tiles, movedTile]
      };
    case DashboardActionTypes.DELETE_TILE:
      const deletingOrder = state.tiles.find(tile => tile.index === action.payload).order;
      const filteredTiles = state.tiles.filter(tile => tile.index !== action.payload);
      const newTiles = [];
      filteredTiles.forEach(tile => {
        if (tile.order > deletingOrder) {
          newTiles.push({ ...tile, order: tile.order - 1 });
        }
        else {
          newTiles.push(tile);
        }
      });
      return {
        ...state,
        tiles: newTiles
      };
    case DashboardActionTypes.CLEAR_DASHBOARD:
      return {
        ...state,
        tiles: []
      };
    case DashboardActionTypes.SET_TILE_CHART:
      // https://redux.js.org/recipes/structuring-reducers/immutable-update-patterns
      return {
        ...state,
        tiles: state.tiles.map(item => {
          // leave other tiles untouched
          if (item.index !== action.payload.index) { return item; }
          // set chart type
          return { ...item, chart: action.payload.chartType };
        })
      };
    case DashboardActionTypes.SET_TILE_REPORT:
      return {
        ...state,
        tiles: state.tiles.map(item => {
          // leave other tiles untouched
          if (item.index !== action.payload.index) { return item; }
          // set report type
          return { ...item, report: action.payload.reportType };
        })
      };
    case DashboardActionTypes.SET_TILE_DATE_RANGE:
      return {
        ...state,
        tiles: state.tiles.map(item => {
          // leave other tiles untouched
          if (item.index !== action.payload.index) { return item; }
          // set date range
          return { ...item, dateRange: action.payload.date };
        })
      };
    case DashboardActionTypes.SAVE:
      return {
        ...state
      };
    default:
      return state;
  }
}
