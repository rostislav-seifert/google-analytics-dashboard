import { AccountSelectionActions, AccountSelectionActionTypes } from '../actions/account-selection.actions';

export interface AccountSelectionState {
  id: string;
  name: string;
  accountId: string;
  accountName: string;
}

const initialState: AccountSelectionState = {
  id: '',
  name: '',
  accountId: '',
  accountName: ''
};

export function accountSelectionReducer(state: AccountSelectionState = initialState,
                                        action: AccountSelectionActions): AccountSelectionState {
  switch (action.type) {
    case AccountSelectionActionTypes.INIT:
      return {
        ...state,
        id: action.payload.id,
        name: action.payload.name,
        accountId: action.payload.accountId,
        accountName: action.payload.accountName
      };
    case AccountSelectionActionTypes.SELECT_ACCOUNT:
      return {
        ...state,
        id: action.payload.id,
        name: action.payload.name,
        accountId: action.payload.accountId,
        accountName: action.payload.accountName
      };
    case AccountSelectionActionTypes.SAVE:
      return {
        ...state
      };
    default:
      return state;
  }
}
