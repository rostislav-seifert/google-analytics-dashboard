import { LoginState } from './reducers/login.reducer';
import { AccountSelectionState } from './reducers/account-selection.reducer';
import { DashboardState } from './reducers/dashboard.reducer';

export interface AppState {
  logIn: LoginState;
  accountSelection: AccountSelectionState;
  dashboard: DashboardState;
}
